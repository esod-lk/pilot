var gulp = require('gulp');

var templateCache = require('gulp-angular-templatecache');
var htmlmin = require('gulp-htmlmin');
var concat = require('gulp-concat');
var inline = require('gulp-inline');
var filter = require("gulp-filter");
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var ngModuleFilter = require("gulp-ng-module-filter");
var replace = require('gulp-replace');
var rename = require('gulp-rename');

gulp.task('default', ['inline'], function () {
});

function ngModuleFilterTask(module) {
    var src = [
        'src/module/**/*Module.js',
        'src/module/**/*Service.js',
        'src/module/**/*Ctrl.js',
        'src/module/**/*.html',
        'src/ng-libs/**/*.js'
    ];
    return gulp.src(src)
        .pipe(ngModuleFilter({appModule: module}))
        .pipe(gulp.dest("build/modules/" + module))
        .pipe(filter("**/*.js"))
        .pipe(concat('modules.js'))
        .pipe(uglify())
        .pipe(gulp.dest("build/modules/" + module))
        ;
}

var concatAllModuleScript = function (module) {
    return gulp.src(['build/modules/' + module + '/**/*.html'])
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(templateCache({module: module}))
        .pipe(gulp.dest("build/modules/" + module));
};

var renameModule = function (module) {
    return gulp.src('src/index.html')
        .pipe(replace('@MODULE_NAME@', module))
        .pipe(rename('build/all/' + module + '.html'))
        .pipe(gulp.dest('./'));
};

var concatAllScript = function (module) {
    var src = [
        'build/modules/' + module + '/modules.js',
        'build/modules/' + module + '/templates.js',
        'src/bootstrap.js'
    ];
    return gulp.src(src)
        .pipe(concat(module + '.js'))
        .pipe(replace('@MODULE_NAME@', module))
        .pipe(uglify())
        .pipe(gulp.dest("build/all"));
};

var createModuleTasks = function (module) {
    gulp.task(module + 'ModuleFilter', [], function () {
        return ngModuleFilterTask(module);
    });
    gulp.task(module + 'ConcatAllModuleScript', [module + 'ModuleFilter'], function () {
        return concatAllModuleScript(module);
    });
    gulp.task(module + 'Rename', [module + 'ConcatAllModuleScript'], function () {
        return renameModule(module);
    });
    gulp.task(module + 'ConcatAllScript', [module + 'Rename'], function () {
        return concatAllScript(module);
    });

};

gulp.task('copyJS', function () {
    return gulp.src('src/asserts/js/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('build/all/asserts/js'));
});

gulp.task('copyCss', function () {
    //return gulp.src('src/asserts/css/**/*.css')
    return gulp.src('src/asserts/css/**/*.*')
        //.pipe(minifyCss())
        .pipe(gulp.dest('build/distr/asserts/css'));
        //.pipe(gulp.dest('build/all/asserts/css'));
});

var modules = ['f0420890'];

var inlineDependenceTasks = ['copyJS', 'copyCss'];

for (var i = 0; i < modules.length; i++) {
    createModuleTasks(modules[i]);
    inlineDependenceTasks.unshift(modules[i] + 'ConcatAllScript');
}

gulp.task('inline', inlineDependenceTasks, function () {
    return gulp.src('build/all/*.html')
        .pipe(inline())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('build/distr'));
});

gulp.task('watch', ['inline'], function () {
    gulp.watch('src/**/*.*', function () {
        gulp.start('inline');
        console.log('\n');
    });
});