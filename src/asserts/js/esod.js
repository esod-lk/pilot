/*! jQuery UI - v1.12.1 - 2016-12-21
 * http://jqueryui.com
 * Includes: keycode.js, widgets/datepicker.js
 * Copyright jQuery Foundation and other contributors; Licensed MIT */

(function (t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery)
})(function (t) {
    function e(t) {
        for (var e, i; t.length && t[0] !== document;) {
            if (e = t.css("position"), ("absolute" === e || "relative" === e || "fixed" === e) && (i = parseInt(t.css("zIndex"), 10), !isNaN(i) && 0 !== i))return i;
            t = t.parent()
        }
        return 0
    }

    function i() {
        this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {
            closeText: "Done",
            prevText: "Prev",
            nextText: "Next",
            currentText: "Today",
            monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            weekHeader: "Wk",
            dateFormat: "mm/dd/yy",
            firstDay: 0,
            isRTL: !1,
            showMonthAfterYear: !1,
            yearSuffix: ""
        }, this._defaults = {
            showOn: "focus",
            showAnim: "fadeIn",
            showOptions: {},
            defaultDate: null,
            appendText: "",
            buttonText: "...",
            buttonImage: "",
            buttonImageOnly: !1,
            hideIfNoPrevNext: !1,
            navigationAsDateFormat: !1,
            gotoCurrent: !1,
            changeMonth: !1,
            changeYear: !1,
            yearRange: "c-10:c+10",
            showOtherMonths: !1,
            selectOtherMonths: !1,
            showWeek: !1,
            calculateWeek: this.iso8601Week,
            shortYearCutoff: "+10",
            minDate: null,
            maxDate: null,
            duration: "fast",
            beforeShowDay: null,
            beforeShow: null,
            onSelect: null,
            onChangeMonthYear: null,
            onClose: null,
            numberOfMonths: 1,
            showCurrentAtPos: 0,
            stepMonths: 1,
            stepBigMonths: 12,
            altField: "",
            altFormat: "",
            constrainInput: !0,
            showButtonPanel: !1,
            autoSize: !1,
            disabled: !1
        }, t.extend(this._defaults, this.regional[""]), this.regional.en = t.extend(!0, {}, this.regional[""]), this.regional["en-US"] = t.extend(!0, {}, this.regional.en), this.dpDiv = s(t("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))
    }

    function s(e) {
        var i = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
        return e.on("mouseout", i, function () {
            t(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).removeClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).removeClass("ui-datepicker-next-hover")
        }).on("mouseover", i, n)
    }

    function n() {
        t.datepicker._isDisabledDatepicker(a.inline ? a.dpDiv.parent()[0] : a.input[0]) || (t(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), t(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).addClass("ui-datepicker-next-hover"))
    }

    function o(e, i) {
        t.extend(e, i);
        for (var s in i)null == i[s] && (e[s] = i[s]);
        return e
    }

    t.ui = t.ui || {}, t.ui.version = "1.12.1", t.ui.keyCode = {
        BACKSPACE: 8,
        COMMA: 188,
        DELETE: 46,
        DOWN: 40,
        END: 35,
        ENTER: 13,
        ESCAPE: 27,
        HOME: 36,
        LEFT: 37,
        PAGE_DOWN: 34,
        PAGE_UP: 33,
        PERIOD: 190,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
    }, t.extend(t.ui, {datepicker: {version: "1.12.1"}});
    var a;
    t.extend(i.prototype, {
        markerClassName: "hasDatepicker",
        maxRows: 4,
        _widgetDatepicker: function () {
            return this.dpDiv
        },
        setDefaults: function (t) {
            return o(this._defaults, t || {}), this
        },
        _attachDatepicker: function (e, i) {
            var s, n, o;
            s = e.nodeName.toLowerCase(), n = "div" === s || "span" === s, e.id || (this.uuid += 1, e.id = "dp" + this.uuid), o = this._newInst(t(e), n), o.settings = t.extend({}, i || {}), "input" === s ? this._connectDatepicker(e, o) : n && this._inlineDatepicker(e, o)
        },
        _newInst: function (e, i) {
            var n = e[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1");
            return {
                id: n,
                input: e,
                selectedDay: 0,
                selectedMonth: 0,
                selectedYear: 0,
                drawMonth: 0,
                drawYear: 0,
                inline: i,
                dpDiv: i ? s(t("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv
            }
        },
        _connectDatepicker: function (e, i) {
            var s = t(e);
            i.append = t([]), i.trigger = t([]), s.hasClass(this.markerClassName) || (this._attachments(s, i), s.addClass(this.markerClassName).on("keydown", this._doKeyDown).on("keypress", this._doKeyPress).on("keyup", this._doKeyUp), this._autoSize(i), t.data(e, "datepicker", i), i.settings.disabled && this._disableDatepicker(e))
        },
        _attachments: function (e, i) {
            var s, n, o, a = this._get(i, "appendText"), r = this._get(i, "isRTL");
            i.append && i.append.remove(), a && (i.append = t("<span class='" + this._appendClass + "'>" + a + "</span>"), e[r ? "before" : "after"](i.append)), e.off("focus", this._showDatepicker), i.trigger && i.trigger.remove(), s = this._get(i, "showOn"), ("focus" === s || "both" === s) && e.on("focus", this._showDatepicker), ("button" === s || "both" === s) && (n = this._get(i, "buttonText"), o = this._get(i, "buttonImage"), i.trigger = t(this._get(i, "buttonImageOnly") ? t("<img/>").addClass(this._triggerClass).attr({
                src: o,
                alt: n,
                title: n
            }) : t("<button type='button'></button>").addClass(this._triggerClass).html(o ? t("<img/>").attr({
                src: o,
                alt: n,
                title: n
            }) : n)), e[r ? "before" : "after"](i.trigger), i.trigger.on("click", function () {
                return t.datepicker._datepickerShowing && t.datepicker._lastInput === e[0] ? t.datepicker._hideDatepicker() : t.datepicker._datepickerShowing && t.datepicker._lastInput !== e[0] ? (t.datepicker._hideDatepicker(), t.datepicker._showDatepicker(e[0])) : t.datepicker._showDatepicker(e[0]), !1
            }))
        },
        _autoSize: function (t) {
            if (this._get(t, "autoSize") && !t.inline) {
                var e, i, s, n, o = new Date(2009, 11, 20), a = this._get(t, "dateFormat");
                a.match(/[DM]/) && (e = function (t) {
                    for (i = 0, s = 0, n = 0; t.length > n; n++)t[n].length > i && (i = t[n].length, s = n);
                    return s
                }, o.setMonth(e(this._get(t, a.match(/MM/) ? "monthNames" : "monthNamesShort"))), o.setDate(e(this._get(t, a.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - o.getDay())), t.input.attr("size", this._formatDate(t, o).length)
            }
        },
        _inlineDatepicker: function (e, i) {
            var s = t(e);
            s.hasClass(this.markerClassName) || (s.addClass(this.markerClassName).append(i.dpDiv), t.data(e, "datepicker", i), this._setDate(i, this._getDefaultDate(i), !0), this._updateDatepicker(i), this._updateAlternate(i), i.settings.disabled && this._disableDatepicker(e), i.dpDiv.css("display", "block"))
        },
        _dialogDatepicker: function (e, i, s, n, a) {
            var r, l, h, c, u, d = this._dialogInst;
            return d || (this.uuid += 1, r = "dp" + this.uuid, this._dialogInput = t("<input type='text' id='" + r + "' style='position: absolute; top: -100px; width: 0px;'/>"), this._dialogInput.on("keydown", this._doKeyDown), t("body").append(this._dialogInput), d = this._dialogInst = this._newInst(this._dialogInput, !1), d.settings = {}, t.data(this._dialogInput[0], "datepicker", d)), o(d.settings, n || {}), i = i && i.constructor === Date ? this._formatDate(d, i) : i, this._dialogInput.val(i), this._pos = a ? a.length ? a : [a.pageX, a.pageY] : null, this._pos || (l = document.documentElement.clientWidth, h = document.documentElement.clientHeight, c = document.documentElement.scrollLeft || document.body.scrollLeft, u = document.documentElement.scrollTop || document.body.scrollTop, this._pos = [l / 2 - 100 + c, h / 2 - 150 + u]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), d.settings.onSelect = s, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), t.blockUI && t.blockUI(this.dpDiv), t.data(this._dialogInput[0], "datepicker", d), this
        },
        _destroyDatepicker: function (e) {
            var i, s = t(e), n = t.data(e, "datepicker");
            s.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), t.removeData(e, "datepicker"), "input" === i ? (n.append.remove(), n.trigger.remove(), s.removeClass(this.markerClassName).off("focus", this._showDatepicker).off("keydown", this._doKeyDown).off("keypress", this._doKeyPress).off("keyup", this._doKeyUp)) : ("div" === i || "span" === i) && s.removeClass(this.markerClassName).empty(), a === n && (a = null))
        },
        _enableDatepicker: function (e) {
            var i, s, n = t(e), o = t.data(e, "datepicker");
            n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !1, o.trigger.filter("button").each(function () {
                this.disabled = !1
            }).end().filter("img").css({
                opacity: "1.0",
                cursor: ""
            })) : ("div" === i || "span" === i) && (s = n.children("." + this._inlineClass), s.children().removeClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), this._disabledInputs = t.map(this._disabledInputs, function (t) {
                return t === e ? null : t
            }))
        },
        _disableDatepicker: function (e) {
            var i, s, n = t(e), o = t.data(e, "datepicker");
            n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !0, o.trigger.filter("button").each(function () {
                this.disabled = !0
            }).end().filter("img").css({
                opacity: "0.5",
                cursor: "default"
            })) : ("div" === i || "span" === i) && (s = n.children("." + this._inlineClass), s.children().addClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), this._disabledInputs = t.map(this._disabledInputs, function (t) {
                return t === e ? null : t
            }), this._disabledInputs[this._disabledInputs.length] = e)
        },
        _isDisabledDatepicker: function (t) {
            if (!t)return !1;
            for (var e = 0; this._disabledInputs.length > e; e++)if (this._disabledInputs[e] === t)return !0;
            return !1
        },
        _getInst: function (e) {
            try {
                return t.data(e, "datepicker")
            } catch (i) {
                throw"Missing instance data for this datepicker"
            }
        },
        _optionDatepicker: function (e, i, s) {
            var n, a, r, l, h = this._getInst(e);
            return 2 === arguments.length && "string" == typeof i ? "defaults" === i ? t.extend({}, t.datepicker._defaults) : h ? "all" === i ? t.extend({}, h.settings) : this._get(h, i) : null : (n = i || {}, "string" == typeof i && (n = {}, n[i] = s), h && (this._curInst === h && this._hideDatepicker(), a = this._getDateDatepicker(e, !0), r = this._getMinMaxDate(h, "min"), l = this._getMinMaxDate(h, "max"), o(h.settings, n), null !== r && void 0 !== n.dateFormat && void 0 === n.minDate && (h.settings.minDate = this._formatDate(h, r)), null !== l && void 0 !== n.dateFormat && void 0 === n.maxDate && (h.settings.maxDate = this._formatDate(h, l)), "disabled"in n && (n.disabled ? this._disableDatepicker(e) : this._enableDatepicker(e)), this._attachments(t(e), h), this._autoSize(h), this._setDate(h, a), this._updateAlternate(h), this._updateDatepicker(h)), void 0)
        },
        _changeDatepicker: function (t, e, i) {
            this._optionDatepicker(t, e, i)
        },
        _refreshDatepicker: function (t) {
            var e = this._getInst(t);
            e && this._updateDatepicker(e)
        },
        _setDateDatepicker: function (t, e) {
            var i = this._getInst(t);
            i && (this._setDate(i, e), this._updateDatepicker(i), this._updateAlternate(i))
        },
        _getDateDatepicker: function (t, e) {
            var i = this._getInst(t);
            return i && !i.inline && this._setDateFromField(i, e), i ? this._getDate(i) : null
        },
        _doKeyDown: function (e) {
            var i, s, n, o = t.datepicker._getInst(e.target), a = !0, r = o.dpDiv.is(".ui-datepicker-rtl");
            if (o._keyEvent = !0, t.datepicker._datepickerShowing)switch (e.keyCode) {
                case 9:
                    t.datepicker._hideDatepicker(), a = !1;
                    break;
                case 13:
                    return n = t("td." + t.datepicker._dayOverClass + ":not(." + t.datepicker._currentClass + ")", o.dpDiv), n[0] && t.datepicker._selectDay(e.target, o.selectedMonth, o.selectedYear, n[0]), i = t.datepicker._get(o, "onSelect"), i ? (s = t.datepicker._formatDate(o), i.apply(o.input ? o.input[0] : null, [s, o])) : t.datepicker._hideDatepicker(), !1;
                case 27:
                    t.datepicker._hideDatepicker();
                    break;
                case 33:
                    t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(o, "stepBigMonths") : -t.datepicker._get(o, "stepMonths"), "M");
                    break;
                case 34:
                    t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(o, "stepBigMonths") : +t.datepicker._get(o, "stepMonths"), "M");
                    break;
                case 35:
                    (e.ctrlKey || e.metaKey) && t.datepicker._clearDate(e.target), a = e.ctrlKey || e.metaKey;
                    break;
                case 36:
                    (e.ctrlKey || e.metaKey) && t.datepicker._gotoToday(e.target), a = e.ctrlKey || e.metaKey;
                    break;
                case 37:
                    (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, r ? 1 : -1, "D"), a = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(o, "stepBigMonths") : -t.datepicker._get(o, "stepMonths"), "M");
                    break;
                case 38:
                    (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, -7, "D"), a = e.ctrlKey || e.metaKey;
                    break;
                case 39:
                    (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, r ? -1 : 1, "D"), a = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(o, "stepBigMonths") : +t.datepicker._get(o, "stepMonths"), "M");
                    break;
                case 40:
                    (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, 7, "D"), a = e.ctrlKey || e.metaKey;
                    break;
                default:
                    a = !1
            } else 36 === e.keyCode && e.ctrlKey ? t.datepicker._showDatepicker(this) : a = !1;
            a && (e.preventDefault(), e.stopPropagation())
        },
        _doKeyPress: function (e) {
            var i, s, n = t.datepicker._getInst(e.target);
            return t.datepicker._get(n, "constrainInput") ? (i = t.datepicker._possibleChars(t.datepicker._get(n, "dateFormat")), s = String.fromCharCode(null == e.charCode ? e.keyCode : e.charCode), e.ctrlKey || e.metaKey || " " > s || !i || i.indexOf(s) > -1) : void 0
        },
        _doKeyUp: function (e) {
            var i, s = t.datepicker._getInst(e.target);
            if (s.input.val() !== s.lastVal)try {
                i = t.datepicker.parseDate(t.datepicker._get(s, "dateFormat"), s.input ? s.input.val() : null, t.datepicker._getFormatConfig(s)), i && (t.datepicker._setDateFromField(s), t.datepicker._updateAlternate(s), t.datepicker._updateDatepicker(s))
            } catch (n) {
            }
            return !0
        },
        _showDatepicker: function (i) {
            if (i = i.target || i, "input" !== i.nodeName.toLowerCase() && (i = t("input", i.parentNode)[0]), !t.datepicker._isDisabledDatepicker(i) && t.datepicker._lastInput !== i) {
                var s, n, a, r, l, h, c;
                s = t.datepicker._getInst(i), t.datepicker._curInst && t.datepicker._curInst !== s && (t.datepicker._curInst.dpDiv.stop(!0, !0), s && t.datepicker._datepickerShowing && t.datepicker._hideDatepicker(t.datepicker._curInst.input[0])), n = t.datepicker._get(s, "beforeShow"), a = n ? n.apply(i, [i, s]) : {}, a !== !1 && (o(s.settings, a), s.lastVal = null, t.datepicker._lastInput = i, t.datepicker._setDateFromField(s), t.datepicker._inDialog && (i.value = ""), t.datepicker._pos || (t.datepicker._pos = t.datepicker._findPos(i), t.datepicker._pos[1] += i.offsetHeight), r = !1, t(i).parents().each(function () {
                    return r |= "fixed" === t(this).css("position"), !r
                }), l = {
                    left: t.datepicker._pos[0],
                    top: t.datepicker._pos[1]
                }, t.datepicker._pos = null, s.dpDiv.empty(), s.dpDiv.css({
                    position: "absolute",
                    display: "block",
                    top: "-1000px"
                }), t.datepicker._updateDatepicker(s), l = t.datepicker._checkOffset(s, l, r), s.dpDiv.css({
                    position: t.datepicker._inDialog && t.blockUI ? "static" : r ? "fixed" : "absolute",
                    display: "none",
                    left: l.left + "px",
                    top: l.top + "px"
                }), s.inline || (h = t.datepicker._get(s, "showAnim"), c = t.datepicker._get(s, "duration"), s.dpDiv.css("z-index", e(t(i)) + 1), t.datepicker._datepickerShowing = !0, t.effects && t.effects.effect[h] ? s.dpDiv.show(h, t.datepicker._get(s, "showOptions"), c) : s.dpDiv[h || "show"](h ? c : null), t.datepicker._shouldFocusInput(s) && s.input.trigger("focus"), t.datepicker._curInst = s))
            }
        },
        _updateDatepicker: function (e) {
            this.maxRows = 4, a = e, e.dpDiv.empty().append(this._generateHTML(e)), this._attachHandlers(e);
            var i, s = this._getNumberOfMonths(e), o = s[1], r = 17, l = e.dpDiv.find("." + this._dayOverClass + " a");
            l.length > 0 && n.apply(l.get(0)), e.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), o > 1 && e.dpDiv.addClass("ui-datepicker-multi-" + o).css("width", r * o + "em"), e.dpDiv[(1 !== s[0] || 1 !== s[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), e.dpDiv[(this._get(e, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), e === t.datepicker._curInst && t.datepicker._datepickerShowing && t.datepicker._shouldFocusInput(e) && e.input.trigger("focus"), e.yearshtml && (i = e.yearshtml, setTimeout(function () {
                i === e.yearshtml && e.yearshtml && e.dpDiv.find("select.ui-datepicker-year:first").replaceWith(e.yearshtml), i = e.yearshtml = null
            }, 0))
        },
        _shouldFocusInput: function (t) {
            return t.input && t.input.is(":visible") && !t.input.is(":disabled") && !t.input.is(":focus")
        },
        _checkOffset: function (e, i, s) {
            var n = e.dpDiv.outerWidth(), o = e.dpDiv.outerHeight(), a = e.input ? e.input.outerWidth() : 0, r = e.input ? e.input.outerHeight() : 0, l = document.documentElement.clientWidth + (s ? 0 : t(document).scrollLeft()), h = document.documentElement.clientHeight + (s ? 0 : t(document).scrollTop());
            return i.left -= this._get(e, "isRTL") ? n - a : 0, i.left -= s && i.left === e.input.offset().left ? t(document).scrollLeft() : 0, i.top -= s && i.top === e.input.offset().top + r ? t(document).scrollTop() : 0, i.left -= Math.min(i.left, i.left + n > l && l > n ? Math.abs(i.left + n - l) : 0), i.top -= Math.min(i.top, i.top + o > h && h > o ? Math.abs(o + r) : 0), i
        },
        _findPos: function (e) {
            for (var i, s = this._getInst(e), n = this._get(s, "isRTL"); e && ("hidden" === e.type || 1 !== e.nodeType || t.expr.filters.hidden(e));)e = e[n ? "previousSibling" : "nextSibling"];
            return i = t(e).offset(), [i.left, i.top]
        },
        _hideDatepicker: function (e) {
            var i, s, n, o, a = this._curInst;
            !a || e && a !== t.data(e, "datepicker") || this._datepickerShowing && (i = this._get(a, "showAnim"), s = this._get(a, "duration"), n = function () {
                t.datepicker._tidyDialog(a)
            }, t.effects && (t.effects.effect[i] || t.effects[i]) ? a.dpDiv.hide(i, t.datepicker._get(a, "showOptions"), s, n) : a.dpDiv["slideDown" === i ? "slideUp" : "fadeIn" === i ? "fadeOut" : "hide"](i ? s : null, n), i || n(), this._datepickerShowing = !1, o = this._get(a, "onClose"), o && o.apply(a.input ? a.input[0] : null, [a.input ? a.input.val() : "", a]), this._lastInput = null, this._inDialog && (this._dialogInput.css({
                position: "absolute",
                left: "0",
                top: "-100px"
            }), t.blockUI && (t.unblockUI(), t("body").append(this.dpDiv))), this._inDialog = !1)
        },
        _tidyDialog: function (t) {
            t.dpDiv.removeClass(this._dialogClass).off(".ui-datepicker-calendar")
        },
        _checkExternalClick: function (e) {
            if (t.datepicker._curInst) {
                var i = t(e.target), s = t.datepicker._getInst(i[0]);
                (i[0].id !== t.datepicker._mainDivId && 0 === i.parents("#" + t.datepicker._mainDivId).length && !i.hasClass(t.datepicker.markerClassName) && !i.closest("." + t.datepicker._triggerClass).length && t.datepicker._datepickerShowing && (!t.datepicker._inDialog || !t.blockUI) || i.hasClass(t.datepicker.markerClassName) && t.datepicker._curInst !== s) && t.datepicker._hideDatepicker()
            }
        },
        _adjustDate: function (e, i, s) {
            var n = t(e), o = this._getInst(n[0]);
            this._isDisabledDatepicker(n[0]) || (this._adjustInstDate(o, i + ("M" === s ? this._get(o, "showCurrentAtPos") : 0), s), this._updateDatepicker(o))
        },
        _gotoToday: function (e) {
            var i, s = t(e), n = this._getInst(s[0]);
            this._get(n, "gotoCurrent") && n.currentDay ? (n.selectedDay = n.currentDay, n.drawMonth = n.selectedMonth = n.currentMonth, n.drawYear = n.selectedYear = n.currentYear) : (i = new Date, n.selectedDay = i.getDate(), n.drawMonth = n.selectedMonth = i.getMonth(), n.drawYear = n.selectedYear = i.getFullYear()), this._notifyChange(n), this._adjustDate(s)
        },
        _selectMonthYear: function (e, i, s) {
            var n = t(e), o = this._getInst(n[0]);
            o["selected" + ("M" === s ? "Month" : "Year")] = o["draw" + ("M" === s ? "Month" : "Year")] = parseInt(i.options[i.selectedIndex].value, 10), this._notifyChange(o), this._adjustDate(n)
        },
        _selectDay: function (e, i, s, n) {
            var o, a = t(e);
            t(n).hasClass(this._unselectableClass) || this._isDisabledDatepicker(a[0]) || (o = this._getInst(a[0]), o.selectedDay = o.currentDay = t("a", n).html(), o.selectedMonth = o.currentMonth = i, o.selectedYear = o.currentYear = s, this._selectDate(e, this._formatDate(o, o.currentDay, o.currentMonth, o.currentYear)))
        },
        _clearDate: function (e) {
            var i = t(e);
            this._selectDate(i, "")
        },
        _selectDate: function (e, i) {
            var s, n = t(e), o = this._getInst(n[0]);
            i = null != i ? i : this._formatDate(o), o.input && o.input.val(i), this._updateAlternate(o), s = this._get(o, "onSelect"), s ? s.apply(o.input ? o.input[0] : null, [i, o]) : o.input && o.input.trigger("change"), o.inline ? this._updateDatepicker(o) : (this._hideDatepicker(), this._lastInput = o.input[0], "object" != typeof o.input[0] && o.input.trigger("focus"), this._lastInput = null)
        },
        _updateAlternate: function (e) {
            var i, s, n, o = this._get(e, "altField");
            o && (i = this._get(e, "altFormat") || this._get(e, "dateFormat"), s = this._getDate(e), n = this.formatDate(i, s, this._getFormatConfig(e)), t(o).val(n))
        },
        noWeekends: function (t) {
            var e = t.getDay();
            return [e > 0 && 6 > e, ""]
        },
        iso8601Week: function (t) {
            var e, i = new Date(t.getTime());
            return i.setDate(i.getDate() + 4 - (i.getDay() || 7)), e = i.getTime(), i.setMonth(0), i.setDate(1), Math.floor(Math.round((e - i) / 864e5) / 7) + 1
        },
        parseDate: function (e, i, s) {
            if (null == e || null == i)throw"Invalid arguments";
            if (i = "object" == typeof i ? "" + i : i + "", "" === i)return null;
            var n, o, a, r, l = 0, h = (s ? s.shortYearCutoff : null) || this._defaults.shortYearCutoff, c = "string" != typeof h ? h : (new Date).getFullYear() % 100 + parseInt(h, 10), u = (s ? s.dayNamesShort : null) || this._defaults.dayNamesShort, d = (s ? s.dayNames : null) || this._defaults.dayNames, p = (s ? s.monthNamesShort : null) || this._defaults.monthNamesShort, f = (s ? s.monthNames : null) || this._defaults.monthNames, g = -1, m = -1, _ = -1, v = -1, b = !1, y = function (t) {
                var i = e.length > n + 1 && e.charAt(n + 1) === t;
                return i && n++, i
            }, w = function (t) {
                var e = y(t), s = "@" === t ? 14 : "!" === t ? 20 : "y" === t && e ? 4 : "o" === t ? 3 : 2, n = "y" === t ? s : 1, o = RegExp("^\\d{" + n + "," + s + "}"), a = i.substring(l).match(o);
                if (!a)throw"Missing number at position " + l;
                return l += a[0].length, parseInt(a[0], 10)
            }, k = function (e, s, n) {
                var o = -1, a = t.map(y(e) ? n : s, function (t, e) {
                    return [[e, t]]
                }).sort(function (t, e) {
                    return -(t[1].length - e[1].length)
                });
                if (t.each(a, function (t, e) {
                        var s = e[1];
                        return i.substr(l, s.length).toLowerCase() === s.toLowerCase() ? (o = e[0], l += s.length, !1) : void 0
                    }), -1 !== o)return o + 1;
                throw"Unknown name at position " + l
            }, x = function () {
                if (i.charAt(l) !== e.charAt(n))throw"Unexpected literal at position " + l;
                l++
            };
            for (n = 0; e.length > n; n++)if (b)"'" !== e.charAt(n) || y("'") ? x() : b = !1; else switch (e.charAt(n)) {
                case"d":
                    _ = w("d");
                    break;
                case"D":
                    k("D", u, d);
                    break;
                case"o":
                    v = w("o");
                    break;
                case"m":
                    m = w("m");
                    break;
                case"M":
                    m = k("M", p, f);
                    break;
                case"y":
                    g = w("y");
                    break;
                case"@":
                    r = new Date(w("@")), g = r.getFullYear(), m = r.getMonth() + 1, _ = r.getDate();
                    break;
                case"!":
                    r = new Date((w("!") - this._ticksTo1970) / 1e4), g = r.getFullYear(), m = r.getMonth() + 1, _ = r.getDate();
                    break;
                case"'":
                    y("'") ? x() : b = !0;
                    break;
                default:
                    x()
            }
            if (i.length > l && (a = i.substr(l), !/^\s+/.test(a)))throw"Extra/unparsed characters found in date: " + a;
            if (-1 === g ? g = (new Date).getFullYear() : 100 > g && (g += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (c >= g ? 0 : -100)), v > -1)for (m = 1, _ = v; ;) {
                if (o = this._getDaysInMonth(g, m - 1), o >= _)break;
                m++, _ -= o
            }
            if (r = this._daylightSavingAdjust(new Date(g, m - 1, _)), r.getFullYear() !== g || r.getMonth() + 1 !== m || r.getDate() !== _)throw"Invalid date";
            return r
        },
        ATOM: "yy-mm-dd",
        COOKIE: "D, dd M yy",
        ISO_8601: "yy-mm-dd",
        RFC_822: "D, d M y",
        RFC_850: "DD, dd-M-y",
        RFC_1036: "D, d M y",
        RFC_1123: "D, d M yy",
        RFC_2822: "D, d M yy",
        RSS: "D, d M y",
        TICKS: "!",
        TIMESTAMP: "@",
        W3C: "yy-mm-dd",
        _ticksTo1970: 1e7 * 60 * 60 * 24 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)),
        formatDate: function (t, e, i) {
            if (!e)return "";
            var s, n = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort, o = (i ? i.dayNames : null) || this._defaults.dayNames, a = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort, r = (i ? i.monthNames : null) || this._defaults.monthNames, l = function (e) {
                var i = t.length > s + 1 && t.charAt(s + 1) === e;
                return i && s++, i
            }, h = function (t, e, i) {
                var s = "" + e;
                if (l(t))for (; i > s.length;)s = "0" + s;
                return s
            }, c = function (t, e, i, s) {
                return l(t) ? s[e] : i[e]
            }, u = "", d = !1;
            if (e)for (s = 0; t.length > s; s++)if (d)"'" !== t.charAt(s) || l("'") ? u += t.charAt(s) : d = !1; else switch (t.charAt(s)) {
                case"d":
                    u += h("d", e.getDate(), 2);
                    break;
                case"D":
                    u += c("D", e.getDay(), n, o);
                    break;
                case"o":
                    u += h("o", Math.round((new Date(e.getFullYear(), e.getMonth(), e.getDate()).getTime() - new Date(e.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                    break;
                case"m":
                    u += h("m", e.getMonth() + 1, 2);
                    break;
                case"M":
                    u += c("M", e.getMonth(), a, r);
                    break;
                case"y":
                    u += l("y") ? e.getFullYear() : (10 > e.getFullYear() % 100 ? "0" : "") + e.getFullYear() % 100;
                    break;
                case"@":
                    u += e.getTime();
                    break;
                case"!":
                    u += 1e4 * e.getTime() + this._ticksTo1970;
                    break;
                case"'":
                    l("'") ? u += "'" : d = !0;
                    break;
                default:
                    u += t.charAt(s)
            }
            return u
        },
        _possibleChars: function (t) {
            var e, i = "", s = !1, n = function (i) {
                var s = t.length > e + 1 && t.charAt(e + 1) === i;
                return s && e++, s
            };
            for (e = 0; t.length > e; e++)if (s)"'" !== t.charAt(e) || n("'") ? i += t.charAt(e) : s = !1; else switch (t.charAt(e)) {
                case"d":
                case"m":
                case"y":
                case"@":
                    i += "0123456789";
                    break;
                case"D":
                case"M":
                    return null;
                case"'":
                    n("'") ? i += "'" : s = !0;
                    break;
                default:
                    i += t.charAt(e)
            }
            return i
        },
        _get: function (t, e) {
            return void 0 !== t.settings[e] ? t.settings[e] : this._defaults[e]
        },
        _setDateFromField: function (t, e) {
            if (t.input.val() !== t.lastVal) {
                var i = this._get(t, "dateFormat"), s = t.lastVal = t.input ? t.input.val() : null, n = this._getDefaultDate(t), o = n, a = this._getFormatConfig(t);
                try {
                    o = this.parseDate(i, s, a) || n
                } catch (r) {
                    s = e ? "" : s
                }
                t.selectedDay = o.getDate(), t.drawMonth = t.selectedMonth = o.getMonth(), t.drawYear = t.selectedYear = o.getFullYear(), t.currentDay = s ? o.getDate() : 0, t.currentMonth = s ? o.getMonth() : 0, t.currentYear = s ? o.getFullYear() : 0, this._adjustInstDate(t)
            }
        },
        _getDefaultDate: function (t) {
            return this._restrictMinMax(t, this._determineDate(t, this._get(t, "defaultDate"), new Date))
        },
        _determineDate: function (e, i, s) {
            var n = function (t) {
                var e = new Date;
                return e.setDate(e.getDate() + t), e
            }, o = function (i) {
                try {
                    return t.datepicker.parseDate(t.datepicker._get(e, "dateFormat"), i, t.datepicker._getFormatConfig(e))
                } catch (s) {
                }
                for (var n = (i.toLowerCase().match(/^c/) ? t.datepicker._getDate(e) : null) || new Date, o = n.getFullYear(), a = n.getMonth(), r = n.getDate(), l = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, h = l.exec(i); h;) {
                    switch (h[2] || "d") {
                        case"d":
                        case"D":
                            r += parseInt(h[1], 10);
                            break;
                        case"w":
                        case"W":
                            r += 7 * parseInt(h[1], 10);
                            break;
                        case"m":
                        case"M":
                            a += parseInt(h[1], 10), r = Math.min(r, t.datepicker._getDaysInMonth(o, a));
                            break;
                        case"y":
                        case"Y":
                            o += parseInt(h[1], 10), r = Math.min(r, t.datepicker._getDaysInMonth(o, a))
                    }
                    h = l.exec(i)
                }
                return new Date(o, a, r)
            }, a = null == i || "" === i ? s : "string" == typeof i ? o(i) : "number" == typeof i ? isNaN(i) ? s : n(i) : new Date(i.getTime());
            return a = a && "Invalid Date" == "" + a ? s : a, a && (a.setHours(0), a.setMinutes(0), a.setSeconds(0), a.setMilliseconds(0)), this._daylightSavingAdjust(a)
        },
        _daylightSavingAdjust: function (t) {
            return t ? (t.setHours(t.getHours() > 12 ? t.getHours() + 2 : 0), t) : null
        },
        _setDate: function (t, e, i) {
            var s = !e, n = t.selectedMonth, o = t.selectedYear, a = this._restrictMinMax(t, this._determineDate(t, e, new Date));
            t.selectedDay = t.currentDay = a.getDate(), t.drawMonth = t.selectedMonth = t.currentMonth = a.getMonth(), t.drawYear = t.selectedYear = t.currentYear = a.getFullYear(), n === t.selectedMonth && o === t.selectedYear || i || this._notifyChange(t), this._adjustInstDate(t), t.input && t.input.val(s ? "" : this._formatDate(t))
        },
        _getDate: function (t) {
            var e = !t.currentYear || t.input && "" === t.input.val() ? null : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
            return e
        },
        _attachHandlers: function (e) {
            var i = this._get(e, "stepMonths"), s = "#" + e.id.replace(/\\\\/g, "\\");
            e.dpDiv.find("[data-handler]").map(function () {
                var e = {
                    prev: function () {
                        t.datepicker._adjustDate(s, -i, "M")
                    }, next: function () {
                        t.datepicker._adjustDate(s, +i, "M")
                    }, hide: function () {
                        t.datepicker._hideDatepicker()
                    }, today: function () {
                        t.datepicker._gotoToday(s)
                    }, selectDay: function () {
                        return t.datepicker._selectDay(s, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1
                    }, selectMonth: function () {
                        return t.datepicker._selectMonthYear(s, this, "M"), !1
                    }, selectYear: function () {
                        return t.datepicker._selectMonthYear(s, this, "Y"), !1
                    }
                };
                t(this).on(this.getAttribute("data-event"), e[this.getAttribute("data-handler")])
            })
        },
        _generateHTML: function (t) {
            var e, i, s, n, o, a, r, l, h, c, u, d, p, f, g, m, _, v, b, y, w, k, x, C, D, T, I, M, P, S, N, H, A, z, O, E, W, F, L, R = new Date, Y = this._daylightSavingAdjust(new Date(R.getFullYear(), R.getMonth(), R.getDate())), B = this._get(t, "isRTL"), j = this._get(t, "showButtonPanel"), q = this._get(t, "hideIfNoPrevNext"), K = this._get(t, "navigationAsDateFormat"), U = this._getNumberOfMonths(t), V = this._get(t, "showCurrentAtPos"), X = this._get(t, "stepMonths"), $ = 1 !== U[0] || 1 !== U[1], G = this._daylightSavingAdjust(t.currentDay ? new Date(t.currentYear, t.currentMonth, t.currentDay) : new Date(9999, 9, 9)), J = this._getMinMaxDate(t, "min"), Q = this._getMinMaxDate(t, "max"), Z = t.drawMonth - V, te = t.drawYear;
            if (0 > Z && (Z += 12, te--), Q)for (e = this._daylightSavingAdjust(new Date(Q.getFullYear(), Q.getMonth() - U[0] * U[1] + 1, Q.getDate())), e = J && J > e ? J : e; this._daylightSavingAdjust(new Date(te, Z, 1)) > e;)Z--, 0 > Z && (Z = 11, te--);
            for (t.drawMonth = Z, t.drawYear = te, i = this._get(t, "prevText"), i = K ? this.formatDate(i, this._daylightSavingAdjust(new Date(te, Z - X, 1)), this._getFormatConfig(t)) : i, s = this._canAdjustMonth(t, -1, te, Z) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (B ? "e" : "w") + "'>" + i + "</span></a>" : q ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (B ? "e" : "w") + "'>" + i + "</span></a>", n = this._get(t, "nextText"), n = K ? this.formatDate(n, this._daylightSavingAdjust(new Date(te, Z + X, 1)), this._getFormatConfig(t)) : n, o = this._canAdjustMonth(t, 1, te, Z) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (B ? "w" : "e") + "'>" + n + "</span></a>" : q ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (B ? "w" : "e") + "'>" + n + "</span></a>", a = this._get(t, "currentText"), r = this._get(t, "gotoCurrent") && t.currentDay ? G : Y, a = K ? this.formatDate(a, r, this._getFormatConfig(t)) : a, l = t.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(t, "closeText") + "</button>", h = j ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (B ? l : "") + (this._isInRange(t, r) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + a + "</button>" : "") + (B ? "" : l) + "</div>" : "", c = parseInt(this._get(t, "firstDay"), 10), c = isNaN(c) ? 0 : c, u = this._get(t, "showWeek"), d = this._get(t, "dayNames"), p = this._get(t, "dayNamesMin"), f = this._get(t, "monthNames"), g = this._get(t, "monthNamesShort"), m = this._get(t, "beforeShowDay"), _ = this._get(t, "showOtherMonths"), v = this._get(t, "selectOtherMonths"), b = this._getDefaultDate(t), y = "", k = 0; U[0] > k; k++) {
                for (x = "", this.maxRows = 4, C = 0; U[1] > C; C++) {
                    if (D = this._daylightSavingAdjust(new Date(te, Z, t.selectedDay)), T = " ui-corner-all", I = "", $) {
                        if (I += "<div class='ui-datepicker-group", U[1] > 1)switch (C) {
                            case 0:
                                I += " ui-datepicker-group-first", T = " ui-corner-" + (B ? "right" : "left");
                                break;
                            case U[1] - 1:
                                I += " ui-datepicker-group-last", T = " ui-corner-" + (B ? "left" : "right");
                                break;
                            default:
                                I += " ui-datepicker-group-middle", T = ""
                        }
                        I += "'>"
                    }
                    for (I += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + T + "'>" + (/all|left/.test(T) && 0 === k ? B ? o : s : "") + (/all|right/.test(T) && 0 === k ? B ? s : o : "") + this._generateMonthYearHeader(t, Z, te, J, Q, k > 0 || C > 0, f, g) + "</div><table class='ui-datepicker-calendar'><thead>" + "<tr>", M = u ? "<th class='ui-datepicker-week-col'>" + this._get(t, "weekHeader") + "</th>" : "", w = 0; 7 > w; w++)P = (w + c) % 7, M += "<th scope='col'" + ((w + c + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + ">" + "<span title='" + d[P] + "'>" + p[P] + "</span></th>";
                    for (I += M + "</tr></thead><tbody>", S = this._getDaysInMonth(te, Z), te === t.selectedYear && Z === t.selectedMonth && (t.selectedDay = Math.min(t.selectedDay, S)), N = (this._getFirstDayOfMonth(te, Z) - c + 7) % 7, H = Math.ceil((N + S) / 7), A = $ ? this.maxRows > H ? this.maxRows : H : H, this.maxRows = A, z = this._daylightSavingAdjust(new Date(te, Z, 1 - N)), O = 0; A > O; O++) {
                        for (I += "<tr>", E = u ? "<td class='ui-datepicker-week-col'>" + this._get(t, "calculateWeek")(z) + "</td>" : "", w = 0; 7 > w; w++)W = m ? m.apply(t.input ? t.input[0] : null, [z]) : [!0, ""], F = z.getMonth() !== Z, L = F && !v || !W[0] || J && J > z || Q && z > Q, E += "<td class='" + ((w + c + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (F ? " ui-datepicker-other-month" : "") + (z.getTime() === D.getTime() && Z === t.selectedMonth && t._keyEvent || b.getTime() === z.getTime() && b.getTime() === D.getTime() ? " " + this._dayOverClass : "") + (L ? " " + this._unselectableClass + " ui-state-disabled" : "") + (F && !_ ? "" : " " + W[1] + (z.getTime() === G.getTime() ? " " + this._currentClass : "") + (z.getTime() === Y.getTime() ? " ui-datepicker-today" : "")) + "'" + (F && !_ || !W[2] ? "" : " title='" + W[2].replace(/'/g, "&#39;") + "'") + (L ? "" : " data-handler='selectDay' data-event='click' data-month='" + z.getMonth() + "' data-year='" + z.getFullYear() + "'") + ">" + (F && !_ ? "&#xa0;" : L ? "<span class='ui-state-default'>" + z.getDate() + "</span>" : "<a class='ui-state-default" + (z.getTime() === Y.getTime() ? " ui-state-highlight" : "") + (z.getTime() === G.getTime() ? " ui-state-active" : "") + (F ? " ui-priority-secondary" : "") + "' href='#'>" + z.getDate() + "</a>") + "</td>", z.setDate(z.getDate() + 1), z = this._daylightSavingAdjust(z);
                        I += E + "</tr>"
                    }
                    Z++, Z > 11 && (Z = 0, te++), I += "</tbody></table>" + ($ ? "</div>" + (U[0] > 0 && C === U[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : ""), x += I
                }
                y += x
            }
            return y += h, t._keyEvent = !1, y
        },
        _generateMonthYearHeader: function (t, e, i, s, n, o, a, r) {
            var l, h, c, u, d, p, f, g, m = this._get(t, "changeMonth"), _ = this._get(t, "changeYear"), v = this._get(t, "showMonthAfterYear"), b = "<div class='ui-datepicker-title'>", y = "";
            if (o || !m)y += "<span class='ui-datepicker-month'>" + a[e] + "</span>"; else {
                for (l = s && s.getFullYear() === i, h = n && n.getFullYear() === i, y += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", c = 0; 12 > c; c++)(!l || c >= s.getMonth()) && (!h || n.getMonth() >= c) && (y += "<option value='" + c + "'" + (c === e ? " selected='selected'" : "") + ">" + r[c] + "</option>");
                y += "</select>"
            }
            if (v || (b += y + (!o && m && _ ? "" : "&#xa0;")), !t.yearshtml)if (t.yearshtml = "", o || !_)b += "<span class='ui-datepicker-year'>" + i + "</span>"; else {
                for (u = this._get(t, "yearRange").split(":"), d = (new Date).getFullYear(), p = function (t) {
                    var e = t.match(/c[+\-].*/) ? i + parseInt(t.substring(1), 10) : t.match(/[+\-].*/) ? d + parseInt(t, 10) : parseInt(t, 10);
                    return isNaN(e) ? d : e
                }, f = p(u[0]), g = Math.max(f, p(u[1] || "")), f = s ? Math.max(f, s.getFullYear()) : f, g = n ? Math.min(g, n.getFullYear()) : g, t.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; g >= f; f++)t.yearshtml += "<option value='" + f + "'" + (f === i ? " selected='selected'" : "") + ">" + f + "</option>";
                t.yearshtml += "</select>", b += t.yearshtml, t.yearshtml = null
            }
            return b += this._get(t, "yearSuffix"), v && (b += (!o && m && _ ? "" : "&#xa0;") + y), b += "</div>"
        },
        _adjustInstDate: function (t, e, i) {
            var s = t.selectedYear + ("Y" === i ? e : 0), n = t.selectedMonth + ("M" === i ? e : 0), o = Math.min(t.selectedDay, this._getDaysInMonth(s, n)) + ("D" === i ? e : 0), a = this._restrictMinMax(t, this._daylightSavingAdjust(new Date(s, n, o)));
            t.selectedDay = a.getDate(), t.drawMonth = t.selectedMonth = a.getMonth(), t.drawYear = t.selectedYear = a.getFullYear(), ("M" === i || "Y" === i) && this._notifyChange(t)
        },
        _restrictMinMax: function (t, e) {
            var i = this._getMinMaxDate(t, "min"), s = this._getMinMaxDate(t, "max"), n = i && i > e ? i : e;
            return s && n > s ? s : n
        },
        _notifyChange: function (t) {
            var e = this._get(t, "onChangeMonthYear");
            e && e.apply(t.input ? t.input[0] : null, [t.selectedYear, t.selectedMonth + 1, t])
        },
        _getNumberOfMonths: function (t) {
            var e = this._get(t, "numberOfMonths");
            return null == e ? [1, 1] : "number" == typeof e ? [1, e] : e
        },
        _getMinMaxDate: function (t, e) {
            return this._determineDate(t, this._get(t, e + "Date"), null)
        },
        _getDaysInMonth: function (t, e) {
            return 32 - this._daylightSavingAdjust(new Date(t, e, 32)).getDate()
        },
        _getFirstDayOfMonth: function (t, e) {
            return new Date(t, e, 1).getDay()
        },
        _canAdjustMonth: function (t, e, i, s) {
            var n = this._getNumberOfMonths(t), o = this._daylightSavingAdjust(new Date(i, s + (0 > e ? e : n[0] * n[1]), 1));
            return 0 > e && o.setDate(this._getDaysInMonth(o.getFullYear(), o.getMonth())), this._isInRange(t, o)
        },
        _isInRange: function (t, e) {
            var i, s, n = this._getMinMaxDate(t, "min"), o = this._getMinMaxDate(t, "max"), a = null, r = null, l = this._get(t, "yearRange");
            return l && (i = l.split(":"), s = (new Date).getFullYear(), a = parseInt(i[0], 10), r = parseInt(i[1], 10), i[0].match(/[+\-].*/) && (a += s), i[1].match(/[+\-].*/) && (r += s)), (!n || e.getTime() >= n.getTime()) && (!o || e.getTime() <= o.getTime()) && (!a || e.getFullYear() >= a) && (!r || r >= e.getFullYear())
        },
        _getFormatConfig: function (t) {
            var e = this._get(t, "shortYearCutoff");
            return e = "string" != typeof e ? e : (new Date).getFullYear() % 100 + parseInt(e, 10), {
                shortYearCutoff: e,
                dayNamesShort: this._get(t, "dayNamesShort"),
                dayNames: this._get(t, "dayNames"),
                monthNamesShort: this._get(t, "monthNamesShort"),
                monthNames: this._get(t, "monthNames")
            }
        },
        _formatDate: function (t, e, i, s) {
            e || (t.currentDay = t.selectedDay, t.currentMonth = t.selectedMonth, t.currentYear = t.selectedYear);
            var n = e ? "object" == typeof e ? e : this._daylightSavingAdjust(new Date(s, i, e)) : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
            return this.formatDate(this._get(t, "dateFormat"), n, this._getFormatConfig(t))
        }
    }), t.fn.datepicker = function (e) {
        if (!this.length)return this;
        t.datepicker.initialized || (t(document).on("mousedown", t.datepicker._checkExternalClick), t.datepicker.initialized = !0), 0 === t("#" + t.datepicker._mainDivId).length && t("body").append(t.datepicker.dpDiv);
        var i = Array.prototype.slice.call(arguments, 1);
        return "string" != typeof e || "isDisabled" !== e && "getDate" !== e && "widget" !== e ? "option" === e && 2 === arguments.length && "string" == typeof arguments[1] ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i)) : this.each(function () {
            "string" == typeof e ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this].concat(i)) : t.datepicker._attachDatepicker(this, e)
        }) : t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i))
    }, t.datepicker = new i, t.datepicker.initialized = !1, t.datepicker.uuid = (new Date).getTime(), t.datepicker.version = "1.12.1", t.datepicker
});

(function () {
    'use strict';

    //$.fn.validator.Constructor.VALIDATORS.pattern = function ($el) {
    //    var patternRegExp = $el.data('pattern');
    //    var regexp = eval('/^' + patternRegExp + '$/g');
    //    var val = $el.val();
    //    if (val == '') {
    //        return [val];
    //    }
    //    return val.match(regexp);
    //};

    $("counter").each(function (index) {
        $(this).text(" " + (index + 1).toString());
    });

    //Задать настройки для  jQuery.ui datepicker
    jQuery(function ($) {
        $.datepicker.regional['ru'] = {
            inline: true,
            showOtherMonths: true,
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            firstDay: 1,
            nextText: 'След',
            prevText: 'Пред',
            dateFormat: 'yy-mm-dd'
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    });

    /*
     * Обработчики вешаются на body, чтобы была возможность обработать события от html-элементов, сгенерированных в js
     * */
    $('body')

        .on('focus', 'div[data-toggle="collapse"] input', function () {
            openTray($(this));
        })

        .on('click', '[data-toggle="collapse"]', function () {
            changeIconType($(this));
            refreshGroupHeader($(this));
        })

        .on('click', '.delCopyButton', function () {
            delClone($(this));
        })

        .on('click', '.addCopyButton', function () {
            addClone($(this));
        })

        .on('change', 'select', function () {
            selectGroupVisible($(this));
        })

        .on('change', 'select[required="true"]', function () {
            setFirstOptionDisabled($(this));
        })

        .on('click', 'input[data-type="xs:date"]', function () {
            $(this).datepicker().show();
        });

    $("input[data-type='xs:date']").datepicker();
    $('select').change();

    // Раскрывает группу элементов при получении фокуса input'ом в заголовке группы
    function openTray($input) {
        var target = $input.closest('div[data-toggle="collapse"]');
        if (!$.isEmptyObject(target) && $(target).attr('aria-expanded') == "false") {
            target.click();
        }
    }

    // Меняет вид иконки на элементе группы в зависимости от её видимости
    function changeIconType($header) {
        var closedGroupIcon = "glyphicon-triangle-right";
        var openedGroupIcon = "glyphicon-triangle-bottom";

        var targetId = $header.attr("aria-controls");
        $('[aria-controls=' + targetId + ']')
            .find('.glyphicon-triangle-right, .glyphicon-triangle-bottom')
            .each(function (target) {
                if (!$(target).hasClass("collapsing")) {
                    $(this).toggleClass(closedGroupIcon);
                    $(this).toggleClass(openedGroupIcon);
                }
            }, $('#' + targetId));
    }

    // Перезаписать значения элементов группы в заголовке группы
    function refreshGroupHeader($activator) {
        var formGroups = $activator.closest('.collapseBlock').children('.blockBody').children('.panel-body').children('.form-group');
        var values = formGroups.map(function (index, element) {
            var $inputField = $(element).find('input');
            if (!$.isEmptyObject($inputField) && $inputField.length) {
                if (!$.isEmptyObject($inputField.val()) && !$.isEmptyObject($inputField.val().trim())) {
                    return $inputField.val().trim();
                }
                return;
            }
            $inputField = $(element).find('select');
            if (!$.isEmptyObject($inputField) && $inputField.length) {
                if (!$.isEmptyObject($inputField.val()) && !$.isEmptyObject($inputField.val().trim())) {
                    var defaultValue = $inputField.children('option.defaultValue').val();
                    if ($inputField.val() != defaultValue) {
                        return $inputField.val().trim();
                    }
                }
            }

        }).get();

        var text = "";
        for (var i = 0; i < values.length; i++) {
            text += values[i];
            if (i + 1 < values.length) {
                text += "; ";
            }
        }
        $activator.closest('.collapseBlock').children('.blockHead').find('input').val(text);
    }

    // Добавляет копию элемента
    function addClone($button) {

        // Программа IE игнорирует атрибут disabled и активирует событие когда не надо.
        if ($button.hasClass('disabled')) {
            return;
        }

        copiesBlockAction($button, function ($clonesGroup, copyNumber) {
            addFieldsBlock($clonesGroup, false, copyNumber);
        });
    }

    // Отображает выборанную группу по значению в select; добавляет атрибуты для генерации xml
    function selectGroupVisible($select) {
        $select.children('option').each(function (select) {
            var groupId = $(this).attr('group');
            if (!$.isEmptyObject(groupId)) {
                if ($(select).val() == $(this).val()) {
                    $('#' + groupId).removeClass('hidden prototype');

                    if ($(select).attr("xml-tag-namespace") != undefined && $(select).attr("xml-tag-name") != undefined
                        && !$.isEmptyObject($(this).attr("xml-tag-namespace")) && !$.isEmptyObject($(this).attr("xml-tag-name"))) {
                        $(select).attr("xml-tag-namespace", $(this).attr("xml-tag-namespace"));
                        $(select).attr("xml-tag-name", $(this).attr("xml-tag-name"));
                    }
                } else {
                    $('#' + groupId).addClass('hidden prototype');
                }
            }
        }, $select);
    }

    // Добалвение минимального количества клонируемых полей
    $('.addCopyButton').each(function () {

        copiesBlockAction($(this), function ($clonesGroup, copyNumber) {
            var min = $clonesGroup.attr('min');
            if (copyNumber < min) {
                for (var i = copyNumber; i < min; i++) {
                    addFieldsBlock($clonesGroup, true, i);
                }
            }
        });
    });

    function copiesBlockAction(activator, updateFunc) {

        var $clonesGroup = activator.closest('.clonesGroup');
        var copyNumber = $clonesGroup.children('.cloneElement').length - 1;

        updateFunc($clonesGroup, copyNumber);

        setDisabledAddButton($clonesGroup, activator);
    }

    function setDisabledAddButton($clonesGroup, activator) {
        var max = $clonesGroup.attr('max');
        var copyNumber = $clonesGroup.children('.cloneElement').length - 1;

        activator = activator ? activator : $clonesGroup.children('.addCopyButton');

        if (max != 'unbounded' && copyNumber >= max) {
            activator.addClass('disabled');
        } else {
            activator.removeClass('disabled');
        }
    }

    function addFieldsBlock($clonesGroup, disableDel, copyNumber) {
        var $newCloneElement = $clonesGroup.children('.prototype')
            .clone()
            .removeClass('hidden prototype');

        var generatedId = createGuid();
        $newCloneElement.attr('cloneId', $clonesGroup.prop('id') + "_" + generatedId);
        $newCloneElement.attr('number', copyNumber);

        var $head = $newCloneElement.children('.blockHead');
        createBlockTitle($head.find('.blockHeadLabel'), $clonesGroup, copyNumber + 1);
        updateCounterVal($newCloneElement, copyNumber + 1);

        var delCopyButton = $head.find('.delCopyButton');
        disableDel ? delCopyButton.addClass('disabled') : delCopyButton.removeClass('disabled');

        $newCloneElement.find('*').each(function () {
            var attrId = $(this).prop('id');
            if (!$.isEmptyObject(attrId)) {
                $(this).prop('id', attrId + '_' + generatedId);
            }
            var attrHref = $(this).attr('href');
            if (!$.isEmptyObject(attrHref)) {
                $(this).attr('href', attrHref + '_' + generatedId);
            }
            var attrAriaControls = $(this).attr('aria-controls');
            if (!$.isEmptyObject(attrAriaControls)) {
                $(this).attr('aria-controls', attrAriaControls + '_' + generatedId);
            }
            var attrGroup = $(this).attr('group');
            if (!$.isEmptyObject(attrGroup)) {
                $(this).attr('group', attrGroup + '_' + generatedId);
            }
            var attrFor = $(this).attr('for');
            if (!$.isEmptyObject(attrFor)) {
                $(this).attr('for', attrFor + '_' + generatedId);
            }

            // Если добавляемое поле - дата, присоединим к нему виджет datepicker
            if (this.tagName.toLowerCase() == 'input' && $(this).attr('data-type') == 'xs:date') {
                $(this).removeClass('hasDatepicker').datepicker();
            }
        });

        $clonesGroup.children('.addCopyButton').before($newCloneElement);
    }

    function delClone($delCopyButton) {

        if ($delCopyButton.hasClass('disabled')) {
            return;
        }

        var $cloneElement = $delCopyButton.closest('.cloneElement');
        var $clonesGroup = $cloneElement.closest('.clonesGroup');

        $cloneElement.remove();

        $clonesGroup.children('.cloneElement').not('.prototype').each(function (index) {
            $(this).attr('number', index);
            var label = $(this).children('.blockHead').find('.blockHeadLabel');
            createBlockTitle(label, $clonesGroup, index + 1);
            updateCounterVal($(this), index + 1);
        });

        setDisabledAddButton($clonesGroup);
    }

    function createBlockTitle($label, $clonesGroup, index) {
        $label.children('text').remove();
        var copyFieldsHeader = "Группа";
        var title = $clonesGroup.attr('title') ? $clonesGroup.attr('title') : copyFieldsHeader;
        $label.append("<text>" + title + ' (' + index + ')' + "</text>");
    }

    function updateCounterVal($cloneElement, index) {
        $cloneElement.find('input[counter]').val(index).attr("value", index);
    }

    function createGuid() {
        function s4() {
            return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1);
        }

        return (s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4());
    }

    // Дизейблим первый элемент из списка select'а
    var setFirstOptionDisabled = function (select) {
        select.find('option').first().attr('disabled', 'disabled');
    };

})();


window.mf2RuleTemplates = function () {

    var operationType = {more: 'больше', less: 'меньше', equally: 'равно'};
    var varyPattern = "%name%";

    /*
     * Сравнить одно значение с другим
     * @param identityAttrs - массив из 2ух значений, где каждое является массивом идентифицирующих элемент атрибутов
     * @param names - массив из 2ух значений, являющихся наименованиями полей
     * @param operationType - массив операций (operationType), при которых правило возвращает true
     * @param $activator - элемент, активирующий правило
     * @param index - индекс элемента, который нужно выбрать из группы при поиске
     * */
    function compareOneAndOne(identityAttrs, names, operationType, $activator, index) {
        if (!$.isArray(operationType)) {
            operationType = [operationType];
        }

        var operationName = createOperationName(operationType);
        var errorTextPattern = "Значение, указанное в поле " + varyPattern + ", должно быть " + operationName + " значению, указанному в поле " + varyPattern;
        var errorText = getErrorText(errorTextPattern, names);

        index = index ? index : 0;
        var $first = mf2.findElement(identityAttrs[0]).eq(index);
        var $second = mf2.findElement(identityAttrs[1]).eq(index);

        if (compareValues(operationType, $first.val(), $second.val())) {
            mf2.delError(mf2.concatJQueryObject([$first, $second]), errorText);
        } else {
            mf2.addError($($activator), errorText);
        }
    }

    /**
     * Сравнить одно значение с суммой других
     * @param identityAttrs - массив из значений, где каждое является массивом идентифицирующих элемент атрибутов
     * @param names - массив из 2ух значений, являющихся наименованиями полей
     * @param operationType - массив операций (operationType), при которых правило возвращает true
     * @param $activator - элемент, активирующий правило
     * @param index - индекс элемента, который нужно выбрать из группы при поиске
     * */
    function compareOneAndSum(identityAttrs, names, operationType, $activator, index) {
        if (!$.isArray(operationType)) {
            operationType = [operationType];
        }

        var operationName = createOperationName(operationType);
        var errorTextPattern = "Значение, указанное в поле " + varyPattern + ", должно быть " + operationName + " " +
            "сумме значений, указанных в полях " + getElementsSumText(names.slice(1));
        var errorText = getErrorText(errorTextPattern, names);

        index = index ? index : 0;

        var $first = mf2.findElement(identityAttrs[0]).eq(index);
        var $elements = getElementsByNames(identityAttrs.slice(1), index);

        if (compareValues(operationType, $first.val(), mf2.getSum($elements))) {
            mf2.delError($first, errorText);
            mf2.delError($elements, errorText);
        } else {
            mf2.addError($($activator), errorText);
        }
    }

    /**
     * Условие: если в поле 1 есть значение, то и в поле 2 должно быть значение.
     * @param identityAttrs - массив из 2ух значений, где каждое является массивом идентифицирующих элемент атрибутов
     * @param names - массив из 2ух значений, являющихся наименованиями полей
     * @param index - индекс элемента, который нужно выбрать из группы при поиске
     * */
    function hasValueOneAndAnother(identityAttrs, names, index) {

        var errorTextPattern = "Если в поле " + varyPattern + " указано значение, то и в поле " + varyPattern + " должно быть указано зачение";
        var errorText = getErrorText(errorTextPattern, names);

        index = index ? index : 0;

        var $first = mf2.findElement(identityAttrs[0]).eq(index);
        var $second = mf2.findElement(identityAttrs[1]).eq(index);

        if (!(hasVal($first.val()) && !hasVal($second.val()))) {
            mf2.delError(mf2.concatJQueryObject([$first, $second]), errorText);
        } else {
            mf2.addError(mf2.concatJQueryObject([$first, $second]), errorText);
        }
    }

    /**
     * Условие: Значение, указанное в поле 1, равно сумме значений, указанных в полях 1 и 2 за вычетом значения,
     * указанного в поле 3
     * @param identityAttrs - массив из значений, где каждое является массивом идентифицирующих элемент атрибутов.
     * @param names - массив из 2ух значений, являющихся наименованиями полей
     * @param operationType - массив операций (operationType), при которых правило возвращает true
     * @param $activator - элемент, активирующий правило
     * @param index - индекс элемента, который нужно выбрать из группы при поиске
     * */
    function compareOneAndSumDiffOne(identityAttrs, names, operationType, $activator, index) {
        if (!$.isArray(operationType)) {
            operationType = [operationType];
        }

        var operationName = createOperationName(operationType);
        var errorTextPattern = "Значение, указанное в поле " + varyPattern + ", должно быть " + operationName + " " +
            "сумме значений, указанных в полях " + getElementsSumText(names.slice(1, -1)) + " за вычетом " +
            "значения, указанного в поле " + names[names.length - 1];
        var errorText = getErrorText(errorTextPattern, names);

        index = index ? index : 0;

        var $first = mf2.findElement(identityAttrs[0]).eq(index);
        var $sumElements = getElementsByNames(identityAttrs.slice(1, -1), index);
        var $diffElement = mf2.findElement(identityAttrs[identityAttrs.length - 1]).eq(index);

        if (compareValues(operationType, $first.val(), mf2.getSum($sumElements) - $diffElement.val())) {
            mf2.delError($first, errorText);
            mf2.delError($sumElements, errorText);
            mf2.delError($diffElement, errorText);
        } else {
            mf2.addError($($activator), errorText);
        }
    }

    // Возвращает массив элементов по массиву имён
    function getElementsByNames(names, index) {
        var $result = $();
        for (var i = 0; i < names.length; i++) {
            $result = $result.add(mf2.findElement(names[i]).eq(index));
        }
        return $result;
    }

    // Возвращает шаблонный текст для суммы элементов
    function getElementsSumText(arr) {
        var result = "";
        for (var i = 0; i < arr.length; i++) {
            result += arr[i];
            if (i == arr.length - 2) {
                result += " и "
            }
            if (i < arr.length - 2) {
                result += ", "
            }
        }
        return result;
    }

    function getErrorText(pattern, values) {

        var i = 0;
        return pattern.replace(new RegExp(varyPattern, 'g'), function () {
            return values[i++];
        });
    }

    function createOperationName(types) {
        var result = "";
        for (var i = 0; i < types.length; i++) {
            if (i != 0) {
                result += " либо "
            }
            result += types[i];
        }
        return result;
    }

    //Сравнение значения target c compared
    function compareValues(targetType, target, compared) {

        var check = false;
        for (var i = 0; i < targetType.length; i++) {
            if (targetType[i] == operationType.more && +target > +compared) {
                check = true;
            }
            if (targetType[i] == operationType.less && +target < +compared) {
                check = true;
            }
            if (targetType[i] == operationType.equally && +target == +compared) {
                check = true;
            }
        }
        return check;
    }

    // true, если все зачения массива отсутствуют либо все зачения есть и отличны от нуля
    function nonXorValues(valArr) {
        if (valArr.length == 0) {
            return false;
        }
        var result = hasVal(valArr.eq(0).val());
        for (var i = 1; i < valArr.length; i++) {
            result ^= hasVal(valArr.eq(i).val());
        }
        return !result;
    }

    function hasVal(val) {
        return !$.isEmptyObject(val) && ((typeof val == "string" && val != "") || (typeof val == "number" && val != 0));
    }

    return {
        operationType: operationType,

        compareOneAndSum: function (identityAttrs, names, operationType, $activator, index) {
            return compareOneAndSum(identityAttrs, names, operationType, $activator, index);
        },

        compareOneAndOne: function (identityAttrs, names, operationType, $activator, index) {
            return compareOneAndOne(identityAttrs, names, operationType, $activator, index);
        },

        hasValueOneAndAnother: function (identityAttrs, names, index) {
            return hasValueOneAndAnother(identityAttrs, names, index);
        },

        compareOneAndSumDiffOne: function (identityAttrs, names, operationType, $activator, index) {
            return compareOneAndSumDiffOne(identityAttrs, names, operationType, $activator, index);
        }
    }

}();

window.mf2 = function () {

    // todo
    // region deprecated Удалить после перевода на новые скрипты форм 0420820, 0420816

    /*
     * Возвращает объект jQuery для искомого поля ввода
     * @param names - массив названий элементов от родительского до искомого
     * @field - объект fieldsCreator, описывающий конкретный элемент DOM. По умолчанию - input.
     *
     * Пример: Section_01 - элемент верхнего уровня; Org_Properties - элемент втрого уровня; Full_Name - искомфй элемент
     * names = ["Section_01", "Org_Properties", "Full_Name"];
     * Вренется результат выполнения строки: $('[xml-tag-name="Section_01"]').find('[xml-tag-name="Org_Properties"]').find('[xml-tag-name="Full_Name"]').find('input')
     * */
    function findElement_old(names, field) {
        if (!$.isArray(names) || names.length < 1) {
            return null;
        }
        field = field ? field : fieldsCreator();

        var $result = $('[' + field.xmlTag + '="' + names[0] + '"]');
        for (var i = 1; i < names.length; i++) {
            $result = $result.find('[' + field.xmlTag + '=\"' + names[i] + '\"]');
        }

        $result = $result.filter(field.elementType);

        if (!field.uniqueId) {
            $result = $result.filter(function () {
                return $(this).prop("id").split(field.separator).length - 1 == names.length;
            });
        }

        if ($result.size() > 1) {
            $result = $result.not('[id *="prototype"]');
        }

        return $result;
    }

    /*
     * Установить обработчик события
     * @param names - массив названий элементов от родительского до искомого для поиска элемента, от которого обрабатывается событие
     * @param func - обработчик события
     * @param fieldType - тип поля ('element' или 'attribute'), element - по умолчанию
     * @param eventType - тип события ('input', 'select', etc), input - по умолчанию
     * @param elementType - тип элемента ('input', 'button', etc), input - по умолчанию
     * */
    var fieldsType = {element: 'element', attribute: 'attribute'};

    function setEventHandler(names, func, fieldType, eventType, elementType, uniqueId) {
        var field = fieldsCreator(fieldType, elementType, uniqueId);
        var element = findElement_old(names, field);
        if ($.isEmptyObject(element)) {
            return;
        }

        element.on(eventType ? eventType : 'input', func);
    }

    function fieldsCreator(fieldType, elementType, uniqueId) {
        var field = {};

        if (fieldType == fieldsType.attribute) {
            field.fieldType = fieldsType.attribute;
            field.xmlTag = "xml-tag-attr";
            field.separator = "_attr_";
        } else {
            field.fieldType = fieldsType.element;
            field.xmlTag = "xml-tag-name";
            field.separator = "_elt_";
        }
        field.elementType = elementType ? elementType : "input";
        field.uniqueId = uniqueId ? uniqueId : false;

        return field;
    }

    /**
     * Добавить обработчик атрибута "Required"
     * */
    function addRequiredHandler($element) {
        var requiredTextError = "Данное поле обязательно для заполнения";

        //jquery не дает получить значение атрибута required, только его наличие.
        var required = $element[0].getAttribute('required');

        if (required == 'true' || required == '') {
            $element.on(mf2.getInputEvent(), handler);
        } else {
            $element.off('input', handler);
        }

        function handler() {
            // функция jquery off не работает, поэтому в обработчике используется дополнитлеьное условия для проверки события
            var required = $element[0].getAttribute('required');
            if ($.isEmptyObject($(this).val()) && (required == 'true' || required == '')) {
                addError($(this), requiredTextError);
            } else {
                delError($(this), requiredTextError);
            }
        }
    }

    /**
     * Получить сумму значений input'ов из массивов
     * @param arr - масссив input'ов
     */
    function getSum_old(arr) {
        var result = 0;
        for (var i = 0; i < arr.length; i++) {
            result = result + +arr[i].val();
        }
        return result;
    }

    //endregion

    /**
     * Возвращает jquery объект с нужынм списком
     * @param names - массив значений атрибута "xml-tag-name" элементов от родительского до искомого.
     * Параметр names необязательно должен содержать всю цепочку значений от верхнего уровня до целевого, но обязательно -
     * идентифицирующего данный элемент.
     */
    function findElement(names) {

        if (!$.isArray(names) || names.length < 1) {
            return null;
        }

        var $elements = $('[xml-tag-name="' + names[0] + '"]');
        for (var i = 1; i < names.length; i++) {
            $elements = $elements.find('[xml-tag-name="' + names[i] + '"]')
        }

        // исключение коллизий
        names.reverse().shift();
        for (i = 0; i < $elements.length; i++) {
            var $parents = $elements.eq(i).parents('[xml-tag-name]');

            for (var j = 0; j < names.length; j++) {
                if ($parents.eq(j).attr('xml-tag-name') != names[j]) {
                    $elements = $elements.not($elements.eq(i));
                }
            }
        }

        var $result = $elements.find('input');
        if ($result.length == 0) {
            return $elements.filter(function () {
                if (this.tagName.toLowerCase() == 'input') {
                    return $(this);
                }
            });
        }
        return $result;
    }

    /*
     * Добавить/удалить текст ошибки
     * @param element - поле или поля input, для которых нужно установить ошибку
     * @param errorText - текст ошибки
     * */
    function addError($element, errorText) {
        setError($element, errorText, typeErrorOperation.addError);
    }

    function delError($element, errorText) {
        setError($element, errorText, typeErrorOperation.delError);
    }

    var typeErrorOperation = {addError: "addError", delError: "delError"};

    function setError($element, errorText, type) {

        for (var i = 0; i < $element.length; i++) {

            var $errorElement = $element.eq(i).siblings(".help-block, .with-errors");
            if ($.isEmptyObject($errorElement)) {
                return;
            }

            if ($.isEmptyObject($errorElement.children()) ||
                (!$.isEmptyObject($errorElement.children()) && !($errorElement.children('ul').length > 0 && $errorElement.children().size() == 1))) {
                $errorElement.html("<ul></ul>");
            }

            var $ul = $errorElement.children('ul');
            var $li = $errorElement.children('ul').children('li');

            for (var j = 0; j < $li.size(); j++) {
                if ($li.eq(j).text() == errorText) {
                    $li.eq(j).remove();
                }
            }

            if (type == typeErrorOperation.addError) {
                $ul.append("<li>" + errorText + "</li>");
            }

            if ($ul.children().size() > 0) {
                $errorElement.closest('div').addClass('has-error');
            } else {
                $errorElement.closest('div').removeClass('has-error');
            }
        }
    }

    /**
     * Получить сумму значений input'ов из массивов
     * @param $arr - масссив input'ов
     */
    function getSum($arr) {
        var result = 0;
        for (var i = 0; i < $arr.length; i++) {
            result = result + +$arr.eq(i).val();
        }
        return result;
    }

    /**
     * Составляет объект jQuery из элементов в массиве
     * @param arr - массив элементов
     * */
    function concatJQueryObject(arr) {
        var $result = $();
        for (var i = 0; i < arr.length; i++) {
            $result = $result.add($(arr[i]));
        }
        return $result;
    }

    /**
     * Возвращает нужное событие в зависимости от того, используется браузер или ie.
     * */
    function getInputEvent() {
        return !window.eventListener ? 'keyup' : 'input';
    }

    function getSelector(attrs) {

        var result = "";
        for (var i = 0; i < attrs.length; i++) {
            result += "[xml-tag-name=\"" + attrs[i] + "\"] ";
        }
        return result;
    }

    /*
     * Функция позволяет перед выполнением правил для конкретного элемента выполнить дополнительные преобразования.
     * */
    function rulesPreProcessor(event, names, funcs) {

        // Ввиду существования коллизий, селектор для события может вернуть больше элементов, чем нужно.
        // Поэтому определим действительные элементы, которые нужно получить для данного массива имён (names) и проверим,
        // находится ли среди них элемент, активировавший событие.
        var checkElement = false;
        var $elements = findElement(names);
        for (var i = 0; i < $elements.length; i++) {
            if (event.target == $elements.get(i)) {
                checkElement = true;
                break;
            }
        }
        if (!checkElement) {
            return;
        }

        if (!$.isArray(funcs)) {
            funcs = [funcs];
        }

        for (i = 0; i < funcs.length; i++) {
            funcs[i](event);
        }

    }

    return {

        // todo
        // region deprecated Удалить после перевода на новые скрипты форм 0420820, 0420816
        findElement_old: function (names, field) {
            return findElement_old(names, field);
        },

        setEventHandler: function (names, func, fieldType, eventType, elementType, uniqueId) {
            return setEventHandler(names, func, fieldType, eventType, elementType, uniqueId);
        },

        fieldsType: fieldsType,

        fieldsCreator: function (fieldType, elementType, uniqueId) {
            return fieldsCreator(fieldType, elementType, uniqueId);
        },

        addRequiredHandler: function ($element) {
            return addRequiredHandler($element);
        },

        getSum_old: function (arr) {
            return getSum_old(arr);
        },

        //endregion

        rules: mf2RuleTemplates,

        addError: function ($element, errorText) {
            return addError($element, errorText);
        },

        delError: function ($element, errorText) {
            return delError($element, errorText);
        },

        getSum: function (arr) {
            return getSum(arr);
        },

        findElement: function (names) {
            return findElement(names);
        },

        concatJQueryObject: function (arr) {
            return concatJQueryObject(arr);
        },

        getInputEvent: function () {
            return getInputEvent();
        },

        getSelector: function (attrs) {
            return getSelector(attrs);
        },

        rulesPreProcessor: function (event, names, funcs) {
            return rulesPreProcessor(event, names, funcs);
        }
    }
}();


(function () {
    'use strict';

    var getMaxInclusiveTextError = function (maximum) {
        return "Значение должно быть меньше либо равно " + maximum;
    };

    var getMaxExclusiveTextError = function (maximum) {
        return "Значение должно быть меньше " + maximum;
    };

    var getMinInclusiveTextError = function (minimum) {
        return "Значение должно быть больше либо равно " + minimum;
    };

    var getMinExclusiveTextError = function (minimum) {
        return "Значение должно быть больше " + minimum;
    };

    var getMaxTextError = function (maximum) {
        return "Количество символов должно быть меньше " + maximum;
    };

    var getMinTextError = function (minimum) {
        return "Количество символов должно быть больше " + minimum;
    };

    var getLengthTextError = function (total) {
        return "Количество символов должно быть равно " + total;
    };

    var getFractionTextError = function (fractionDig) {
        return "Количество цифр после запятой не может превышать " + fractionDig;
    };

    var minusAtLastTextError = "Число не может оканчиваться минусом";
    var dotAtLastTextError = "Число не может оканчиваться точкой";
    var integerTextError = "Значение поля должно быть целым числом";
    var decimalTextError = "Значение поля должно быть числом";
    var innTextError = "Введено некорректное значение ИНН";
    var ogrnTextError = "Введено некорректное значение ОГРН";
    var requiredTextError = "Поле обязательно для заполнения";
    var commonFormatTextError = "Значение поля имеет неправильный формат";
    var patternCommonTextError = "Значение поля должно удовлетворять шаблону";

    $('body')

        .on(mf2.getInputEvent(), 'input[data-type="xs:integer"]', function () {
            updateValue($(this));
            trimNonInteger($(this));
            validateRequired($(this));
            validateIntegerFormat($(this));
            validateTotalDigits($(this));
            validateMinExclusive($(this));
            validateMaxExclusive($(this));
            validateMinInclusive($(this));
            validateMaxInclusive($(this));
        })

        .on(mf2.getInputEvent(), 'input[data-type="xs:decimal"]', function () {
            updateValue($(this));
            trimNonDecimal($(this));
            validateRequired($(this));
            validateDecimalFormat($(this));
            validateTotalDigits($(this));
            validateFractionDigits($(this));
            validateMinExclusive($(this));
            validateMaxExclusive($(this));
            validateMinInclusive($(this));
            validateMaxInclusive($(this));
        })

        .on(mf2.getInputEvent(), 'input[data-type="xs:string"]', function () {
            updateValue($(this));
            validateRequired($(this));
            validatePattern($(this));
            validateMaxLength($(this));
            validateMinLength($(this));
            validateLength($(this));
        })

        .on('blur', 'input[data-check="INN"]', function () {
            updateValue($(this));
            validateRequired($(this));
            validateInnFormat($(this));
        })

        .on('blur', 'input[data-check="OGRN"]', function () {
            updateValue($(this));
            validateRequired($(this));
            validateOgrnFormat($(this));
        })

        .on('change', 'select', function () {
            updateValue($(this));
            validateRequired($(this));
        });

    // При изменении свойтсва value атрибут value почему-то меняется не всегда.
    function updateValue($element) {
        $element.attr("value", $element.val());
    }

    // Проверяет, удовлетворяет ли значение value числовому шаблону: 0 или знаковое число, начинающееся с 1 с точкой или без.
    function isDecimal(value) {
        return /^(\-?\d+(\.\d+)?)?$/.test(value);
    }

    // Проверяет, удовлетворяет ли значение value числовому шаблону: 0 или знаковое число, начинающееся с 1.
    function isInteger(value) {
        return /^0?$|^\-?[1-9]+\d*$/.test(value);
    }

    // Удаляет из значения элемента все символы, не удовлетворяющие шаблону "целое число". В случае одного минуса -
    // оставляет его в значении и добавлет ошибку.
    function trimNonInteger($element) {

        if (!isInteger($element.val())) {
            var aNumber = [];
            var aElement = $element.val().split('');
            for (var i = 0; i < aElement.length; i++) {
                // Оставляем символ, если это:
                // - 1ый символ и он - цифра или минус;
                // - не первый символ и цифра;
                if ((aNumber.length == 0 && /^\d|-$/.test(aElement[i])) || (aNumber.length > 0 && /^\d$/.test(aElement[i]))) {
                    aNumber.push(aElement[i]);
                }
            }
            $element.val(aNumber.join(''));
            updateValue($(this));
            if ($element.val() == '-') {
                mf2.addError($element, minusAtLastTextError);
                return false;
            }
        }
        mf2.delError($element, minusAtLastTextError);
        return true;
    }

    // Удаляет из значения элемента все символы, не удовлетворяющие шаблону "число". В случае одного минуса - оставляет
    // его в значении и добавлет ошибку.
    function trimNonDecimal($element) {

        if (!isDecimal($element.val())) {
            var aNumber = [];
            var aElement = $element.val().split('');
            var hasDot = false;
            for (var i = 0; i < aElement.length; i++) {
                // Оставляем символ, если это:
                // - 1ый символ и он - цифра или минус;
                // - не первый символ и цифра;
                // - не первый символ и точка, котороая появилась в первый раз
                if ((aNumber.length == 0 && /^\d|-$/.test(aElement[i])) ||
                    (aNumber.length > 0 && (/^\d$/.test(aElement[i]) || (/^\.$/.test(aElement[i]) && !hasDot)))) {
                    if (/^\.$/.test(aElement[i])) {
                        hasDot = true;
                    }
                    aNumber.push(aElement[i]);
                }
            }
            $element.val(aNumber.join(''));
            updateValue($(this));

            if ($element.val() == '-') {
                mf2.addError($element, minusAtLastTextError);
                return false;
            }
            if (/\.$/.test($element.val())) {
                mf2.addError($element, dotAtLastTextError);
                return false;
            }
        }
        mf2.delError($element, minusAtLastTextError);
        mf2.delError($element, dotAtLastTextError);
        return true;
    }

    // Зачение element'а должно быть меньше атрибута "max-inclusive"
    function validateMaxInclusive($element) {

        var maxInclusive = $element.attr('max-inclusive');
        if ($.isEmptyObject(maxInclusive)) {
            return;
        }

        var value = $element.val();

        if (isInteger(value) && isInteger(maxInclusive)) {
            if (+value > +maxInclusive) {
                mf2.addError($element, getMaxInclusiveTextError(maxInclusive));
            } else {
                mf2.delError($element, getMaxInclusiveTextError(maxInclusive));
            }
        }
    }

    // Зачение element'а должно быть больше атрибута "min-inclusive"
    function validateMinInclusive($element) {

        var minInclusive = $element.attr('min-inclusive');
        if ($.isEmptyObject(minInclusive)) {
            return;
        }

        var value = $element.val();

        if (isInteger(value) && isInteger(minInclusive)) {
            if (!$.isEmptyObject(value) && +value < +minInclusive) {
                mf2.addError($element, getMinInclusiveTextError(minInclusive));
            } else {
                mf2.delError($element, getMinInclusiveTextError(minInclusive));
            }
        }
    }

    // Зачение element'а должно быть меньше либо равно атрибуту "max-exclusive"
    function validateMaxExclusive($element) {

        var maxExclusive = $element.attr('max-exclusive');
        if ($.isEmptyObject(maxExclusive)) {
            return;
        }
        var value = $element.val();

        if (isInteger(value) && isInteger(maxExclusive)) {
            if (+value >= +maxExclusive) {
                mf2.addError($element, getMaxExclusiveTextError(maxExclusive));
            } else {
                mf2.delError($element, getMaxExclusiveTextError(maxExclusive));
            }
        }
    }

    // Зачение element'а должно быть больше либо равно атрибуту "min-exclusive"
    function validateMinExclusive($element) {

        var minExclusive = $element.attr('min-exclusive');
        if ($.isEmptyObject(minExclusive)) {
            return;
        }

        var value = $element.val();

        if (isInteger(value) && isInteger(minExclusive)) {
            if (!$.isEmptyObject(value) && +value <= +minExclusive) {
                mf2.addError($element, getMinExclusiveTextError(minExclusive));
            } else {
                mf2.delError($element, getMinExclusiveTextError(minExclusive));
            }
        }
    }

    // Количество символов значения element'а должно быть меньше либо равно атрибуту "total-digits"
    function validateTotalDigits($element) {

        var totalDigits = $element.attr('total-digits');
        if ($.isEmptyObject(totalDigits)) {
            return;
        }

        var value = $element.val();

        if (isInteger(totalDigits)) {
            if (value.length > +totalDigits) {
                mf2.addError($element, getMaxTextError(totalDigits));
            } else {
                mf2.delError($element, getMaxTextError(totalDigits));
            }
        }
    }

    // Количество символов значения element'а после запятой должно быть меньше либо равно атрибуту "fraction-digits"
    function validateFractionDigits($element) {

        var fractionDigits = $element.attr('fraction-digits');
        if ($.isEmptyObject(fractionDigits)) {
            return;
        }

        var value = $element.val();

        if (isDecimal(value) && isInteger(fractionDigits) && value.lastIndexOf('.') != -1) {

            var fractionValue = value.substring(value.indexOf('.') + 1);
            if (fractionValue.length > +fractionDigits) {
                mf2.addError($element, getFractionTextError(fractionDigits));
            } else {
                mf2.delError($element, getFractionTextError(fractionDigits));
            }
        }
    }

    //  Проверка обязательности
    function validateRequired($element) {

        var required = $element.attr('esod-required');
        if (required == undefined) {
            return;
        }

        var value = $element.val();
        if ($.isEmptyObject(value)) {
            mf2.addError($element, requiredTextError);
        } else {
            mf2.delError($element, requiredTextError);
        }
    }

    //  Проверка паттерна
    function validatePattern($element) {

        var pattern = $element.attr('data-esod-pattern');
        var dataEsodPatternError = $element.attr('data-esod-pattern-error');
        if ($.isEmptyObject(pattern)) {
            return;
        }
        var patternTextError = patternCommonTextError;
        if (!$.isEmptyObject(dataEsodPatternError)) {
            patternTextError = patternTextError + ": \"" + dataEsodPatternError + "\"";
        }

        var regexp = eval('/^' + pattern.trim() + '$/g');
        var value = $element.val();

        if ($.isEmptyObject(value) || regexp.test(value)) {
            mf2.delError($element, patternTextError);
        } else {
            mf2.addError($element, patternTextError);
        }
    }

    // Значение element'а должно быть integer
    function validateIntegerFormat($element) {

        if (isInteger($element.val())) {
            mf2.delError($element, integerTextError);
        } else {
            mf2.addError($element, integerTextError);
        }
    }

    // Значение element'а должно быть decimal
    function validateDecimalFormat($element) {

        if (isDecimal($element.val())) {
            mf2.delError($element, decimalTextError);
        } else {
            mf2.addError($element, decimalTextError);
        }
    }

    // Количество символов значения element'а должно быть равно атрибуту "length"
    function validateLength($element) {

        var length = $element.attr('length');
        if ($.isEmptyObject(length)) {
            return;
        }

        var value = $element.val();

        if (!$.isEmptyObject(value) && value.length != +length) {
            mf2.addError($element, getLengthTextError(length));
        } else {
            mf2.delError($element, getLengthTextError(length));
        }
    }

    // Количество символов значения element'а должно быть больше либо равно атрибуту "min-length"
    function validateMinLength($element) {

        var minLength = $element.attr('min-length');
        if ($.isEmptyObject(minLength)) {
            return;
        }

        var value = $element.val();
        if (!$.isEmptyObject(value) && value.length < +minLength) {
            mf2.addError($element, getMinTextError(minLength));
        } else {
            mf2.delError($element, getMinTextError(minLength));
        }

    }

    // Количество символов значения element'а должно быть больше либо равно атрибуту "max-length"
    function validateMaxLength($element) {

        var maxLength = $element.attr('max-length');
        if ($.isEmptyObject(maxLength)) {
            return;
        }

        var value = $element.val();

        if (!$.isEmptyObject(maxLength)) {
            if (value.length > +maxLength) {
                mf2.addError($element, getMaxTextError(maxLength));
            } else {
                mf2.delError($element, getMaxTextError(maxLength));
            }
        }
    }

    function validateInnFormat($inn) {
        if (isValidFormat($inn.val())) {
            mf2.delError($inn, innTextError);
        } else {
            mf2.addError($inn, innTextError);
        }

        function isValidFormat(inn) {
            // проверяет, что в строке либо 10 либо 12 цифр
            if ($.isEmptyObject(inn) || !/^\d{10}$|^\d{12}$/.test(inn)) {
                return false;
            }
            inn = inn.split('').map(Number);
            var checkNumbers = [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8];

            if (inn.length == 12) {
                var first = checkNumbers.shift();
                if (!checkRemainderOfDivision()) {
                    return false;
                }

                checkNumbers.unshift(first);
                return checkRemainderOfDivision();

            } else if (inn.length == 10) {
                checkNumbers = checkNumbers.slice(2);
                return checkRemainderOfDivision();
            }
            return false;

            function checkRemainderOfDivision() {
                var checkSum = 0;
                for (var i = 0; i < checkNumbers.length; i++) {
                    checkSum += checkNumbers[i] * inn[i];
                }
                return checkSum % 11 % 10 == inn[i];
            }
        }
    }

    function validateOgrnFormat($ogrn) {
        if (isValidFormat($ogrn.val())) {
            mf2.delError($ogrn, ogrnTextError);
        } else {
            mf2.addError($ogrn, ogrnTextError);
        }

        function isValidFormat(ogrn) {
            if ($.isEmptyObject(ogrn) || !/^\d{13}$/.test(ogrn)) {
                return false;
            }
            var control = ogrn.slice(-1);
            ogrn = ogrn.slice(0, -1);
            return ogrn % 11 % 10 == control;
        }
    }

})();

(function () {
    'use strict';

    $('body')
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_01_01', 'Debt']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_01_01', 'Debt'],
                [rule1]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_01_01', 'Not_Repaid_Debt']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_01_01', 'Not_Repaid_Debt'],
                [rule1]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_01_02', 'Interest_On_Report_Date']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_01_02', 'Interest_On_Report_Date'],
                [rule2]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_01_02', 'Not_Repaid_Interest']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_01_02', 'Not_Repaid_Interest'],
                [rule2]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_01_04', 'Not_Made_Contracts']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_01_04', 'Not_Made_Contracts'],
                [rule3, rule4]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_01_04', 'In_Preferential_Period']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_01_04', 'In_Preferential_Period'],
                [rule3]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_01_04', 'Out_Preferential_Period']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_01_04', 'Out_Preferential_Period'],
                [rule3]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_01_04', 'Debtors']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_01_04', 'Debtors'],
                [rule4]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'On_Report_Date', 'Total']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'On_Report_Date', 'Total'],
                [rule5, rule6, rule7, rule13]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'On_Report_Date', 'Main_Debt']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'On_Report_Date', 'Main_Debt'],
                [rule5, rule7, rule11]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'On_Report_Date', 'Interests']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'On_Report_Date', 'Interests'],
                [rule6, rule7, rule12]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Total']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Total'],
                [rule8, rule9, rule10, rule13]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Main_Debt']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Main_Debt'],
                [rule8, rule10, rule11]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Interests']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Interests'],
                [rule9, rule10, rule12]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'For_Period']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'For_Period'],
                [rule14, rule16]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'For_Banks', 'For_Period']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'For_Banks', 'For_Period'],
                [rule14]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'Count']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'Count'],
                [rule15]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_01', 'For_Banks', 'Count']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_01', 'For_Banks', 'Count'],
                [rule15]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_02', 'For_Period']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_02', 'For_Period'],
                [rule16]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_03', 'All_Money']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_03', 'All_Money'],
                [rule16, rule17, rule18]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_02_03', 'Contracts']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_02_03', 'Contracts'],
                [rule17, rule18]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_01_01', 'Loan_Amount']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_01_01', 'Loan_Amount'],
                [rule19, rule24]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Sub_Section_02_03', 'Contracts']), function (event) {
            mf2.rulesPreProcessor(event, ['Sub_Section_02_03', 'Contracts'],
                [rule19]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_03', 'Bail_Transport', 'Average']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_03', 'Bail_Transport', 'Average'],
                [rule20a, rule21a, rule22a, rule25a]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_03', 'Other_Bail', 'Average']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_03', 'Other_Bail', 'Average'],
                [rule20b, rule21b, rule22b, rule25b]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_03', 'Bail_Transport', 'All_Money']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_03', 'Bail_Transport', 'All_Money'],
                [rule20a, rule22a, rule23a, rule24, rule26a]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_03', 'Other_Bail', 'All_Money']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_03', 'Other_Bail', 'All_Money'],
                [rule20b, rule22b, rule23b, rule24, rule26b]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_03', 'Bail_Transport', 'Contracts']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_03', 'Bail_Transport', 'Contracts'],
                [rule21a, rule23a, rule25a, rule26a]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_03', 'Other_Bail', 'Contracts']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_03', 'Other_Bail', 'Contracts'],
                [rule21b, rule23b, rule25b, rule26b]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Assets', 'Total']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Assets', 'Total'],
                [rule27, rule32]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Assets', 'Investments']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Assets', 'Investments'],
                [rule27]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Assets', 'Debt']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Assets', 'Debt'],
                [rule27]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Assets', 'Stocks']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Assets', 'Stocks'],
                [rule27]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Assets', 'Fixed_Assets']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Assets', 'Fixed_Assets'],
                [rule27]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Assets', 'Cash', 'Total']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Assets', 'Cash', 'Total'],
                [rule27, rule28]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Assets', 'Other_Assets']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Assets', 'Other_Assets'],
                [rule27]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Assets', 'Cash', 'Account']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Assets', 'Cash', 'Account'],
                [rule28]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Assets', 'Cash', 'Cash_Store']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Assets', 'Cash', 'Cash_Store'],
                [rule28]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Capital', 'Total']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Capital', 'Total'],
                [rule29, rule32]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Capital', 'Paid-in_Capital']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Capital', 'Paid-in_Capital'],
                [rule29]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Capital', 'Reserve_Capital']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Capital', 'Reserve_Capital'],
                [rule29]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Capital', 'Capital_Surplus']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Capital', 'Capital_Surplus'],
                [rule29]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Capital', 'No_Destributed']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Capital', 'No_Destributed'],
                [rule29]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Capital', 'Other_Capital']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Capital', 'Other_Capital'],
                [rule29]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Obligations', 'Total']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Obligations', 'Total'],
                [rule30, rule32]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Obligations', 'Borrowed_Funds', 'Total']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Obligations', 'Borrowed_Funds', 'Total'],
                [rule30, rule31]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Obligations', 'Credit']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Obligations', 'Credit'],
                [rule30]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Obligations', 'Other_Obligations']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Obligations', 'Other_Obligations'],
                [rule30]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_04', 'Obligations', 'Borrowed_Funds', 'Interests']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_04', 'Obligations', 'Borrowed_Funds', 'Interests'],
                [rule31]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Cash', 'Incoming_Balance']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Cash', 'Incoming_Balance'],
                [rule33]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Cash', 'Increase']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Cash', 'Increase'],
                [rule33]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Cash', 'Decrease']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Cash', 'Decrease'],
                [rule33]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Cash', 'Outgoing_Balance']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Cash', 'Outgoing_Balance'],
                [rule33]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Current_Accounts', 'Incoming_Balance']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Current_Accounts', 'Incoming_Balance'],
                [rule34]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Current_Accounts', 'Increase']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Current_Accounts', 'Increase'],
                [rule34]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Current_Accounts', 'Decrease']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Current_Accounts', 'Decrease'],
                [rule34]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Current_Accounts', 'Outgoing_Balance']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Current_Accounts', 'Outgoing_Balance'],
                [rule34]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Currency_Accounts', 'Incoming_Balance']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Currency_Accounts', 'Incoming_Balance'],
                [rule35]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Currency_Accounts', 'Increase']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Currency_Accounts', 'Increase'],
                [rule35]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Currency_Accounts', 'Decrease']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Currency_Accounts', 'Decrease'],
                [rule35]);
        })
        .on(mf2.getInputEvent(), mf2.getSelector(['Section_05', 'Currency_Accounts', 'Outgoing_Balance']), function (event) {
            mf2.rulesPreProcessor(event, ['Section_05', 'Currency_Accounts', 'Outgoing_Balance'],
                [rule35]);
        })

    ;

    /*
     * Список правил
     * */
    function rule1(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_01_01', 'Debt'], ['Sub_Section_02_01_01', 'Not_Repaid_Debt']],
            ["34", "34.1"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule2(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_01_02', 'Interest_On_Report_Date'], ['Sub_Section_02_01_02', 'Not_Repaid_Interest']],
            ["36", "36.1"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule3(event) {
        mf2.rules.compareOneAndSum([['Sub_Section_02_01_04', 'Not_Made_Contracts'], ['Sub_Section_02_01_04', 'In_Preferential_Period'], ['Sub_Section_02_01_04', 'Out_Preferential_Period']],
            ["41", "41.1", "41.2"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule4(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_01_04', 'Not_Made_Contracts'], ['Sub_Section_02_01_04', 'Debtors']],
            ["41", "42"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule5(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_02_01', 'On_Report_Date', 'Total'], ['Sub_Section_02_02_01', 'On_Report_Date', 'Main_Debt']],
            ["49", "47"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule6(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_02_01', 'On_Report_Date', 'Total'], ['Sub_Section_02_02_01', 'On_Report_Date', 'Interests']],
            ["49", "48"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule7(event) {
        mf2.rules.compareOneAndSum([['Sub_Section_02_02_01', 'On_Report_Date', 'Total'], ['Sub_Section_02_02_01', 'On_Report_Date', 'Main_Debt'], ['Sub_Section_02_02_01', 'On_Report_Date', 'Interests']],
            ["49", "47", "48"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule8(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Total'], ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Main_Debt']],
            ["54", "52"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule9(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Total'], ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Interests']],
            ["54", "53"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule10(event) {
        mf2.rules.compareOneAndSum([['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Total'], ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Main_Debt'], ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Interests']],
            ["54", "52", "53"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule11(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_02_01', 'On_Report_Date', 'Main_Debt'], ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Main_Debt']],
            ["47", "52"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule12(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_02_01', 'On_Report_Date', 'Interests'], ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Interests']],
            ["48", "53"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule13(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_02_01', 'On_Report_Date', 'Total'], ['Sub_Section_02_02_01', 'For_Banks', 'On_Report_Date', 'Total']],
            ["49", "54"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule14(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_02_01', 'For_Period'], ['Sub_Section_02_02_01', 'For_Banks', 'For_Period']],
            ["50", "55"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule15(event) {
        mf2.rules.compareOneAndOne([['Sub_Section_02_02_01', 'Count'], ['Sub_Section_02_02_01', 'For_Banks', 'Count']],
            ["51", "56"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule16(event) {
        mf2.rules.compareOneAndSum([['Sub_Section_02_02_03', 'All_Money'], ['Sub_Section_02_02_01', 'For_Period'], ['Sub_Section_02_02_02', 'For_Period']],
            ["60", "50", "58"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule17(event) {
        mf2.rules.hasValueOneAndAnother([['Sub_Section_02_02_03', 'All_Money'], ['Sub_Section_02_02_03', 'Contracts']],
            ["60", "61"],
            getIndex($(event.target)));
    }

    function rule18(event) {
        mf2.rules.hasValueOneAndAnother([['Sub_Section_02_02_03', 'Contracts'], ['Sub_Section_02_02_03', 'All_Money']],
            ["61", "60"],
            getIndex($(event.target)));
    }

    function rule19(event) {
        mf2.rules.hasValueOneAndAnother([['Sub_Section_02_01_01', 'Loan_Amount'], ['Sub_Section_02_03', 'Contracts']],
            ["33", "63"],
            getIndex($(event.target)));
    }

    function rule20a(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Bail_Transport', 'Average'], ['Section_03', 'Bail_Transport', 'All_Money']],
            ["73", "74"],
            getIndex($(event.target)));
    }

    function rule20b(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Other_Bail', 'Average'], ['Section_03', 'Other_Bail', 'All_Money']],
            ["73", "74"],
            getIndex($(event.target)));
    }

    function rule21a(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Bail_Transport', 'Average'], ['Section_03', 'Bail_Transport', 'Contracts']],
            ["73", "75"],
            getIndex($(event.target)));
    }

    function rule21b(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Other_Bail', 'Average'], ['Section_03', 'Other_Bail', 'Contracts']],
            ["73", "75"],
            getIndex($(event.target)));
    }

    function rule22a(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Bail_Transport', 'All_Money'], ['Section_03', 'Bail_Transport', 'Average']],
            ["74", "73"],
            getIndex($(event.target)));
    }

    function rule22b(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Other_Bail', 'All_Money'], ['Section_03', 'Other_Bail', 'Average']],
            ["74", "73"],
            getIndex($(event.target)));
    }

    function rule23a(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Bail_Transport', 'All_Money'], ['Section_03', 'Bail_Transport', 'Contracts']],
            ["74", "75"],
            getIndex($(event.target)));
    }

    function rule23b(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Bail_Transport', 'All_Money'], ['Section_03', 'Bail_Transport', 'Contracts']],
            ["74", "75"],
            getIndex($(event.target)));
    }

    function rule24(event) {
        mf2.rules.compareOneAndSum([['Sub_Section_02_01_01', 'Loan_Amount'], ['Section_03', 'Other_Bail', 'All_Money'], ['Section_03', 'Bail_Transport', 'All_Money']],
            ["33", "74"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule25a(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Bail_Transport', 'Contracts'], ['Section_03', 'Bail_Transport', 'Average']],
            ["75", "73"],
            getIndex($(event.target)));
    }

    function rule25b(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Other_Bail', 'Contracts'], ['Section_03', 'Other_Bail', 'Average']],
            ["75", "73"],
            getIndex($(event.target)));
    }

    function rule26a(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Bail_Transport', 'Contracts'], ['Section_03', 'Bail_Transport', 'All_Money']],
            ["75", "74"],
            getIndex($(event.target)));
    }

    function rule26b(event) {
        mf2.rules.hasValueOneAndAnother([['Section_03', 'Other_Bail', 'Contracts'], ['Section_03', 'Other_Bail', 'All_Money']],
            ["75", "74"],
            getIndex($(event.target)));
    }

    function rule27(event) {
        mf2.rules.compareOneAndSum([['Section_04', 'Assets', 'Total'], ['Section_04', 'Assets', 'Investments'], ['Section_04', 'Assets', 'Debt'],
                ['Section_04', 'Assets', 'Stocks'], ['Section_04', 'Assets', 'Fixed_Assets'], ['Section_04', 'Assets', 'Cash', 'Total'], ['Section_04', 'Assets', 'Other_Assets']],
            ["1", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule28(event) {
        mf2.rules.compareOneAndSum([['Section_04', 'Assets', 'Cash', 'Total'], ['Section_04', 'Assets', 'Cash', 'Account'], ['Section_04', 'Assets', 'Cash', 'Cash_Store']],
            ["1.5", "1.5.1", "1.5.2"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule29(event) {
        mf2.rules.compareOneAndSum([['Section_04', 'Capital', 'Total'], ['Section_04', 'Capital', 'Paid-in_Capital'], ['Section_04', 'Capital', 'Reserve_Capital'],
                ['Section_04', 'Capital', 'Capital_Surplus'], ['Section_04', 'Capital', 'No_Destributed'], ['Section_04', 'Capital', 'Other_Capital']],
            ["2", "2.1", "2.2", "2.3", "2.4", "2.5"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule30(event) {
        mf2.rules.compareOneAndSum([['Section_04', 'Obligations', 'Total'], ['Section_04', 'Obligations', 'Borrowed_Funds', 'Total'], ['Section_04', 'Obligations', 'Credit'], ['Section_04', 'Obligations', 'Other_Obligations']],
            ["3", "3.1", "3.2", "3.3"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule31(event) {
        mf2.rules.compareOneAndOne([['Section_04', 'Obligations', 'Borrowed_Funds', 'Total'], ['Section_04', 'Obligations', 'Borrowed_Funds', 'Interests']],
            ["3.1", "3.1.1"],
            [mf2.rules.operationType.more, mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule32(event) {
        mf2.rules.compareOneAndSum([['Section_04', 'Assets', 'Total'], ['Section_04', 'Capital', 'Total'], ['Section_04', 'Obligations', 'Total']],
            ["1", "2", "3"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule33(event) {
        mf2.rules.compareOneAndSumDiffOne([['Section_05', 'Cash', 'Outgoing_Balance'], ['Section_05', 'Cash', 'Incoming_Balance'], ['Section_05', 'Cash', 'Increase'], ['Section_05', 'Cash', 'Decrease']],
            ["5", "2", "3", "4"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule34(event) {
        mf2.rules.compareOneAndSumDiffOne([['Section_05', 'Current_Accounts', 'Outgoing_Balance'], ['Section_05', 'Current_Accounts', 'Incoming_Balance'], ['Section_05', 'Current_Accounts', 'Increase'], ['Section_05', 'Current_Accounts', 'Decrease']],
            ["5", "2", "3", "4"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    function rule35(event) {
        mf2.rules.compareOneAndSumDiffOne([['Section_05', 'Currency_Accounts', 'Outgoing_Balance'], ['Section_05', 'Currency_Accounts', 'Incoming_Balance'], ['Section_05', 'Currency_Accounts', 'Increase'], ['Section_05', 'Currency_Accounts', 'Decrease']],
            ["5", "2", "3", "4"],
            [mf2.rules.operationType.equally],
            $(event.target),
            getIndex($(event.target)));
    }

    // Получить индекс элемент в зависимости от выбранного значения в главном select'е
    var reportDate = ["First_Quarter", "Half_Year", "Nine_Month", "Pre_Year", "Year"];
    function getIndex($target) {
        for (var i = 0; i < reportDate.length; i++) {
            if (!$.isEmptyObject($target.closest('[xml-tag-name="' + reportDate[i] + '"]').get(0))) {
                return i;
            }
        }
        return undefined;
    }

})();