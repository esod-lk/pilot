(function () {
    'use strict';

    var app = angular.module('f0420890', [
        'common',
        'folderTreeView',
        'filereader',
        'commons.messages',
        'f0420890.Section_01'
    ]);

    var pagesTree = [
        {
            url: '',
            name: 'Выбор периода',
            controller: 'MainCtrl'
        },
        {
            url: 'Section_01',
            name: 'Раздел I. Общие сведения о ломбарде',
            children: [
                {
                    url: 'Sub_Section_01_01',
                    name: 'Подраздел 1. Реквизиты ломбарда',
                    controller:'f0420890.Section_01.Sub_Section_01_01Ctrl'
                },
                {
                    url: 'Sub_Section_01_02',
                    name: 'Подраздел 2. Сведения об обособленных подразделениях ломбарда',
                    controller:'f0420890.Section_01.Sub_Section_01_02Ctrl'
                },
                {
                    url: 'Sub_Section_01_03',
                    name: 'Подраздел 3. Сведения о счетах ломбарда, открытых в кредитных организациях'
                },
                {
                    url: 'Sub_Section_01_04',
                    name: 'Подраздел 4. Сведения о страховых организациях, осуществляющих страхование имущества, ' +
                    'переданного ломбарду в залог и принятого на хранение'
                }
            ]
        },
        {
            url: 'Section_02',
            name: "Раздел II. Основные показатели деятельности ломбарда",
            children: [
                {
                    url: 'Sub_Section_02_01',
                    name: 'Подраздел 1. Сведения о займах'
                },
                {
                    url: 'Sub_Section_02_02',
                    name: 'Подраздел 2. Сведения о денежных средствах, привлеченных ломбардом'
                },
                {
                    url: 'Sub_Section_02_03',
                    name: 'Подраздел 3. Информация о страховании рисков ломбарда, связанных с вещами'
                },
                {
                    url: 'Sub_Section_02_04',
                    name: 'Подраздел 4. Иная информация'
                }
            ]
        },
        {
            url: 'Section_03',
            name: "Раздел III. Средневзвешенные значения полной стоимости потребительских займов ломбарда",
            children: []
        },
        {
            url: 'Section_04',
            name: "Раздел IV. Балансовые показатели",
            children: []
        },
        {
            url: 'Section_05',
            name: "Раздел V. Информация об операциях с денежными средствами",
            children: []
        },
        {
            url: 'letter',
            name: "Сопроводительное письмо",
            children: []
        }
    ];


    var buildStates = function ($stateProvider, pages, parentUrlArray) {
        for (var i = 0; i < pages.length; i++) {
            var page = pages[i];
            var parentUrls;
            if (parentUrlArray == undefined || parentUrlArray == null) {
                parentUrls = [];
            } else {
                parentUrls = angular.copy(parentUrlArray);
            }
            parentUrls.push(page.url);

            page.state = 'root.' + parentUrls.join('/');
            var url = '/' + parentUrls.join('/');
            var templateUrl = 'forms/f0420890' + url + '.html';
            if (page.state == 'root.') {
                page.state = 'root.index';
                templateUrl = 'forms/f0420890/index.html';
            }
            $stateProvider
                .state(page.state, {
                    url: url,
                    controller: page.controller,
                    templateUrl: templateUrl
                });
            if (page.children != undefined) {
                buildStates($stateProvider, page.children, parentUrls);
            }
        }
    };

    app.config(['$stateProvider',
        function ($stateProvider) {
            buildStates($stateProvider, pagesTree);

            $stateProvider
                .state('root.Section_01/Sub_Section_01_02/Subdivision', {
                    url:'/Section_01/Sub_Section_01_02/Subdivision/:subdivisionIndex',
                    templateUrl: 'forms/f0420890/Section_01/Sub_Section_01_02/Subdivision.html',
                    controller:'f0420890.Section_01.Sub_Section_01_02.SubdivisionCtrl'
                })
                .state('print', {
                    url:'/print',
                    templateUrl: 'forms/f0420890/print.html'
                })
                .state('draft', {
                    url:'/draft',
                    templateUrl: 'forms/f0420890/draft.html',
                    controller:"DraftCtrl"
                })
                .state('help', {
                    url:'/help',
                    templateUrl: 'forms/f0420890/help.html'
                });
        }
    ]);

    app.config([
        'formValidationDecorationsProvider',
        'formValidationErrorsProvider',
        function(
            formValidationDecorationsProvider,
            formValidationErrorsProvider
        ) {
            formValidationDecorationsProvider.useBuiltInDecorator('bootstrap');//.useIconLibrary('fontawesome');
            formValidationErrorsProvider.useBuiltInErrorListRenderer('bootstrap');
        }
    ]);

    app.controller("FormCtrl", [
        '$scope',
        '$rootScope',
        '$state',
        'commons.messages',
        function (
            $scope,
            $rootScope,
            $state,
            messages
        ) {
            var currentPage = null;
            var findStates = function() {
                var previousState = null;
                var findInLevel = function(pages){
                    for (var i = 0; i < pages.length; i++) {
                        var page = pages[i];
                         if ($state.$current.name == page.state){
                             $scope.states.previousState = previousState;
                             currentPage = page;
                         } else{
                             previousState = page.state;
                             if (currentPage != null){
                                 $scope.states.nextState = page.state;
                                 return true;
                             }
                         }
                        if (page.children != undefined) {
                            if (findInLevel(page.children)){
                                return true;
                            }
                        }
                        $scope.states.nextState = null;
                    }
                };
                $scope.states ={

                };
                currentPage = null;
                findInLevel($rootScope.treeData);
            };

            findStates();

            var isNoConfirmedStateChange = true;
            $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
                if ($scope.form.$dirty){
                    if (isNoConfirmedStateChange) {
                        messages.dialog.confirm('Внимание', 'Вы начали заполнять поля, но не все заполнены правильно, вы точно хотите ее покинуть?').result
                            .then(function () {
                                isNoConfirmedStateChange = false;
                                $state.go(toState, toParams, options);
                            });
                        event.preventDefault();
                    }else{
                        isNoConfirmedStateChange = true;
                        $scope.form.$dirty = false;
                        if ($scope.form.$valid){
                            currentPage.iconValidType = 'glyphicon glyphicon-ok';
                            currentPage.classValid = 'has-success';
                        }else{
                            currentPage.iconValidType = 'glyphicon glyphicon-remove';
                            currentPage.classValid = 'has-error';
                        }
                    }

                }
            });

            $rootScope.$watch('treeData', function () {
                findStates();
            });

            $scope.$on('$stateChangeSuccess', function () {
                findStates();
            });

            $scope.goNextState = function(){
                if ($scope.states.nextState != null){
                    $state.go($scope.states.nextState);
                }
            };

            $scope.goPreviousState = function(){
                if ($scope.states.nextState != null){
                    $state.go($scope.states.previousState);
                }
            };

            $scope.createXmlAndSign = function() {
                var x2js = new X2JS({stripWhitespaces:true});
                var xml = x2js.json2xml_str({Form_0420890:$rootScope.data.form})
                $rootScope.data.create = {
                    xml : xml
                };

                $state.go('cades');
            };

            $scope.createDraft = function () {
                if ($rootScope.data.meta.period != null){
                    $rootScope.data.form.Form_Data[$rootScope.data.meta.period] = $rootScope.data.periodChoice;
                }

                var x2js = new X2JS({stripWhitespaces:true});
                $rootScope.xml = x2js.json2xml_str({Form_0420890:$rootScope.data.form});


                var saveData = (function () {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    return function (data, fileName) {
                        var blob = new Blob([data], {type: "text/xml"}),
                            url = window.URL.createObjectURL(blob);
                        a.href = url;
                        a.download = fileName;
                        a.click();
                        window.URL.revokeObjectURL(url);
                    };
                }());

                var fileName = "my-download.xml";

                saveData($rootScope.xml, fileName);
            };
        }
    ]);

    app.controller("DraftCtrl", [
        '$scope',
        '$rootScope',
        '$state',
        'FileReader',
        'commons.messages',
        function ($scope, $rootScope, $state, FileReader, messages) {

            $scope.loadDraft = function () {
                var x2js = new X2JS();
                var xml = x2js.xml_str2json($scope.draft);

                function setPeriodChoice(period) {
                    $rootScope.data.meta = {period: period};
                    $rootScope.data.saveData = {};
                    $rootScope.data.periodChoice = xml.Form_0420890.Form_Data[period];
                    delete xml.Form_0420890[period];
                }
                //Повтор кода!!!!
                function saveSection(section) {
                    if (angular.isDefined($rootScope.data.periodChoice[section])) {
                        $rootScope.data.saveData[section] = $rootScope.data.periodChoice[section];
                        delete $rootScope.data.periodChoice[section];
                    }
                }
                //Конец повтора кода!!!!

                try {
                    $rootScope.data.form = xml.Form_0420890;
                    if (angular.isDefined(xml.Form_0420890.Form_Data.First_Quarter)){
                        setPeriodChoice('First_Quarter');
                    } else if (angular.isDefined(xml.Form_0420890.Form_Data.Half_Year)){
                        setPeriodChoice('Half_Year');
                    } else if (angular.isDefined(xml.Form_0420890.Form_Data.Nine_Month)){
                        setPeriodChoice('Nine_Month');
                    } else if (angular.isDefined(xml.Form_0420890.Form_Data.Year)){
                        setPeriodChoice('Year');
                    } else if (angular.isDefined(xml.Form_0420890.Form_Data.Pre_Year)){
                        setPeriodChoice('Pre_Year');
                    } else {
                        $rootScope.data.form.meta = {period: null};
                        $rootScope.data.form.saveData = {};
                    }

                    //Повтор кода!!!!
                    if ($scope.data.meta.period == 'Pre_Year') {
                        $rootScope.treeData = [
                            pagesTree[0],
                            pagesTree[1],
                            pagesTree[3],
                            pagesTree[6]
                        ];
                        saveSection('Section_02');
                        saveSection('Section_04');
                        saveSection('Section_05');
                    } else if ($scope.data.meta.period == 'Year') {
                        $rootScope.treeData = [
                            pagesTree[0],
                            pagesTree[1],
                            pagesTree[2],
                            pagesTree[4],
                            pagesTree[5],
                            pagesTree[6]
                        ];
                        saveSection('Section_03');
                    } else if ($scope.data.meta.period == 'First_Quarter' || $scope.data.meta.period == 'Half_Year' || $scope.data.meta.period == 'Nine_Month'){
                        $rootScope.treeData = pagesTree;
                    }
                    //Конец повтора кода!!!!
                    messages.notify.success('Черновик удачно загружен.');
                    $state.go('root.index')

                 } catch (e) {
                         messages.log.error(e);
                         messages.dialog.error('Загрузить черновик не удалось!',' Проверьте корректность загружаемых данных.\n' + e);
                 }
            };


            $scope.fileNameChanged = function (files) {
                $scope.curFile = {name: "", target: null};

                if (angular.isDefined(files.files)) {
                    $scope.curFile.name = files.files[0].name;
                    $scope.curFile.target = files.files[0];
                } else {
                    $scope.curFile.name = files.value;
                    $scope.curFile.target = files;
                }
                $scope.$apply();
            };

            $scope.loadDraftFromFile = function () {
                if (angular.isDefined($scope.curFile)) {
                    FileReader.readAsText($scope.curFile.target, 'UTF-8', $scope)
                        .then(function  (result) {
                            $scope.draft = result;
                            $scope.loadDraft();
                            $scope.draft = '';
                        });
                }
            }

        }
    ]);


    app.controller("MainCtrl", [
        '$scope',
        '$rootScope',
        function ($scope, $rootScope) {

            $scope.changePeriod = function () {
                delete $rootScope.data.form.Form_Data.Pre_Year;
                delete $rootScope.data.form.Form_Data.Year;
                delete $rootScope.data.form.Form_Data.Nine_Month;
                delete $rootScope.data.form.Form_Data.Half_Year;
                delete $rootScope.data.form.Form_Data.First_Quarter;


                function revertSection(section) {
                    if (angular.isDefined($rootScope.data.saveData[section])) {
                        $rootScope.data.periodChoice[section] = $rootScope.data.saveData[section];
                        delete $rootScope.data.saveData[section];

                    }
                }

                function saveSection(section) {
                    if (angular.isDefined($rootScope.data.periodChoice[section])) {
                        $rootScope.data.saveData[section] = $rootScope.data.periodChoice[section];
                        delete $rootScope.data.periodChoice[section];
                    }
                }

                revertSection('Section_02');
                revertSection('Section_03');
                revertSection('Section_04');
                revertSection('Section_05');


                if ($scope.data.meta.period == 'Pre_Year') {
                    $rootScope.treeData = [
                        pagesTree[0],
                        pagesTree[1],
                        pagesTree[3],
                        pagesTree[6]
                    ];
                    saveSection('Section_02');
                    saveSection('Section_04');
                    saveSection('Section_05');
                } else if ($scope.data.meta.period == 'Year') {
                    $rootScope.treeData = [
                        pagesTree[0],
                        pagesTree[1],
                        pagesTree[2],
                        pagesTree[4],
                        pagesTree[5],
                        pagesTree[6]
                    ];
                    saveSection('Section_03');
                } else if ($scope.data.meta.period == 'First_Quarter' || $scope.data.meta.period == 'Half_Year' || $scope.data.meta.period == 'Nine_Month'){
                    $rootScope.treeData = pagesTree;
                }
            }
        }
    ]);

    app.run([
        '$rootScope',
        '$state',
        function ($rootScope, $state) {
            $rootScope.formName = 'Форма 0420890. Отчет о деятельности ломбарда';
            $rootScope.treeId = {};
            $rootScope.treeData = [pagesTree[0]];
            $state.go(pagesTree[0].state);
            $rootScope.data = {
                form: {
                    "_xmlns":"urn:cbr-ru:e-forms-mf-form:v0.1",
                    Form_Data: {
                    },
                    Covering_Letter: {
                        "Full_Name": null,
                        "INN": null,
                        "OGRN": null,
                        "Leader": {
                            "_Family_Name": null,
                            "_Name": null
                        },
                        "Send_Reason": null,
                        "Doc_Number": null,
                        "Doc_Date": null,
                        "Performer": {
                            "_Family_Name": null,
                            "_Name": null
                        },
                        "Performer_Phone": {
                            "_City_Code": null,
                            "_Phone_Number":null
                        }
                    }
                },
                periodChoice:{
                    "Section_01": {
                        "Sub_Section_01_01": {
                            "Full_Name": null,
                            "Short_Name": null,
                            "OKOPF_Code": null,
                            "INN": null,
                            "OGRN": null,
                            "Location": {
                                "_Federation_Subject": null,
                                "_City": null
                            },
                            "Real_Address": {
                                "_Federation_Subject": null,
                                "_City": null
                            },
                            "Post_Address": {
                                "_Federation_Subject": null,
                                "_City": null
                            },
                            "Phones": {
                                "Phone": {
                                    "_City_Code": null,
                                    "_Phone_Number": null
                                }
                            }
                        },
                        "Sub_Section_01_02": {},
                        "Sub_Section_01_03": null,
                        "Sub_Section_01_04": null
                    },
                    "Section_02": {
                        "Sub_Section_02_01": {
                            "Sub_Section_02_01_01": {
                                "Loan_Amount": "0",
                                "Debt": "0",
                                "Not_Repaid_Debt": "0"
                            },
                            "Sub_Section_02_01_02": {
                                "Interest_For_Period": "0",
                                "Interest_On_Report_Date": "0",
                                "Not_Repaid_Interest": "0"
                            },
                            "Sub_Section_02_01_03": {
                                "Payment_Main_Debt": "0",
                                "Payment_Interest": "0",
                                "Sale_For_Main_Debt": "0",
                                "Sale_For_Interest": "0"
                            },
                            "Sub_Section_02_01_04": {
                                "Not_Made_Contracts": "0",
                                "In_Preferential_Period": "0",
                                "Out_Preferential_Period": "0",
                                "Debtors": "0",
                                "Contracts_For_Period": "0"
                            },
                            "Sub_Section_02_01_05": {
                                "Restructured_Debt": "0",
                                "Described_Debt": "0",
                                "For_Sale": "0"
                            }
                        },
                        "Sub_Section_02_02": {
                            "Sub_Section_02_02_01": {
                                "On_Report_Date": {
                                    "Main_Debt": "0",
                                    "Interests": "0",
                                    "Total": "0"
                                },
                                "For_Period": "0",
                                "Count": "0",
                                "For_Banks": {
                                    "On_Report_Date": {
                                        "Main_Debt": "0",
                                        "Interests": "0",
                                        "Total": "0"
                                    },
                                    "For_Period": "0",
                                    "Count": "0"
                                }
                            },
                            "Sub_Section_02_02_02": {
                                "On_Report_Date": "0",
                                "For_Period": "0",
                                "Count": "0"
                            },
                            "Sub_Section_02_02_03": {
                                "All_Money": "0",
                                "Contracts": "0",
                                "Debt": "0"
                            }
                        },
                        "Sub_Section_02_03": {
                            "Contracts": "0",
                            "Premium": "0",
                            "Insurance": "0",
                            "Events": "0",
                            "Payments": "0"
                        },
                        "Sub_Section_02_04": {
                            "Contracts": "0",
                            "All_Money": "0",
                            "Result": {
                                "Profit": "0"
                            }
                        }
                    },
                    "Section_03": {
                        "Bail_Transport": {
                            "Average": "0",
                            "All_Money": "0",
                            "Contracts": "0"
                        },
                        "Other_Bail": {
                            "Average": "0",
                            "All_Money": "0",
                            "Contracts": "0"
                        }
                    },
                    "Section_04": {
                        "Assets": {
                            "Total": "0",
                            "Investments": "0",
                            "Debt": "0",
                            "Stocks": "0",
                            "Fixed_Assets": "0",
                            "Cash": {
                                "Total": "0",
                                "Account": "0",
                                "Cash_Store": "0"
                            },
                            "Other_Assets": "0"
                        },
                        "Capital": {
                            "Total": "0",
                            "Paid-in_Capital": "0",
                            "Reserve_Capital": "0",
                            "Capital_Surplus": "0",
                            "No_Destributed": "0",
                            "Other_Capital": "0"
                        },
                        "Obligations": {
                            "Total": "0",
                            "Borrowed_Funds": {
                                "Total": "0",
                                "Interests": "0"
                            },
                            "Credit": "0",
                            "Other_Obligations": "0"
                        }
                    },
                    "Section_05": {
                        "Cash": {
                            "Incoming_Balance": "0",
                            "Increase": "0",
                            "Decrease": "0",
                            "Outgoing_Balance": "0"
                        },
                        "Current_Accounts": {
                            "Incoming_Balance": "0",
                            "Increase": "0",
                            "Decrease": "0",
                            "Outgoing_Balance": "0"
                        },
                        "Currency_Accounts": {
                            "Incoming_Balance": "0",
                            "Increase": "0",
                            "Decrease": "0",
                            "Outgoing_Balance": "0"
                        }
                    }
                },
                meta: {
                    period: null
                },
                saveData:{

                }
            };
        }
    ]);

})();