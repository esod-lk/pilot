(function () {
    'use strict';

    var module = angular.module('f0420890.Section_01.Sub_Section_01_01Ctrl', [

    ]);

    module.controller('f0420890.Section_01.Sub_Section_01_01Ctrl',[
        '$scope',
        '$rootScope',
        '$stateParams',
        'common.xmlService',
        function ($scope, $rootScope, $stateParams, xmlService) {
            var Sub_Section_01_02 = $rootScope.data.periodChoice.Section_01.Sub_Section_01_01;

            xmlService.fixLoadedXmlForControllers.fixPhone(Sub_Section_01_02, $scope);
            xmlService.fixLoadedXmlForControllers.addFaxFunction(Sub_Section_01_02, $scope);
            xmlService.fixLoadedXmlForControllers.fixEmail(Sub_Section_01_02, $scope);
       }
    ]);
})();
