(function () {
    'use strict';

    var module = angular.module('f0420890.Section_01.Sub_Section_01_02Ctrl', [
        'f0420890.Section_01.Sub_Section_01_02.SubdivisionCtrl'
    ]);

    module.controller('f0420890.Section_01.Sub_Section_01_02Ctrl',[
        '$scope',
        '$rootScope',
        '$state',
        function ($scope, $rootScope, $state) {
            var page = $rootScope.treeData[1].children[1];
            page.children = [];

            if (!angular.isArray($rootScope.data.periodChoice.Section_01.Sub_Section_01_02.Subdivision)){
                if (angular.isObject($rootScope.data.periodChoice.Section_01.Sub_Section_01_02.Subdivision)){
                    $rootScope.data.periodChoice.Section_01.Sub_Section_01_02.Subdivision = [
                        $rootScope.data.periodChoice.Section_01.Sub_Section_01_02.Subdivision
                    ];
                }else{
                    $rootScope.data.periodChoice.Section_01.Sub_Section_01_02.Subdivision = [];
                }
            }else{
                var Subdivision = $rootScope.data.periodChoice.Section_01.Sub_Section_01_02.Subdivision;
                for (var i=0;i<Subdivision.length;i++){
                    var params = {subdivisionIndex: i};
                    //Subdivision.Sequence_Number = i;todo ???
                    page.children.push({
                        name:"Обособленное подразделение ломбарда (" + (i + 1) +  ")",
                        state:"root.Section_01/Sub_Section_01_02/Subdivision(" + angular.toJson(params) + ")"
                    });
                }
            }




            $scope.addSubdivision = function(){

                var Subdivision = $rootScope.data.periodChoice.Section_01.Sub_Section_01_02.Subdivision;

                Subdivision.push({
                    Phones:[{}],
                    Sequence_Number:Subdivision.length + 1
                });
                var params = {subdivisionIndex: Subdivision.length - 1};
                page.children.push({
                    name:"Обособленное подразделение ломбарда (" + (Subdivision.length) +  ")",
                    state:"root.Section_01/Sub_Section_01_02/Subdivision(" + angular.toJson(params) + ")"
                });
                $state.go('root.Section_01/Sub_Section_01_02/Subdivision', params)
            }
        }
    ]);
})();
