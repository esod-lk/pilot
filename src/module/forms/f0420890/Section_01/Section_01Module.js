(function () {
    'use strict';

    var module = angular.module('f0420890.Section_01', [
        'f0420890.Section_01.Sub_Section_01_01Ctrl',
        'f0420890.Section_01.Sub_Section_01_02Ctrl'
    ]);

})();
