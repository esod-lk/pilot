(function () {
    'use strict';

    var module = angular.module('f0420890.Section_01.Sub_Section_01_02.SubdivisionCtrl', [

    ]);

    module.controller('f0420890.Section_01.Sub_Section_01_02.SubdivisionCtrl',[
        '$scope',
        '$rootScope',
        '$stateParams',
        'common.xmlService',
        function ($scope, $rootScope, $stateParams, xmlService) {
            $scope.Subdivision = $rootScope.data.periodChoice.Section_01.Sub_Section_01_02.Subdivision[$stateParams.subdivisionIndex];

            xmlService.fixLoadedXmlForControllers.fixPhone($scope.Subdivision, $scope);
            xmlService.fixLoadedXmlForControllers.addFaxFunction($scope.Subdivision, $scope);
            xmlService.fixLoadedXmlForControllers.fixEmail($scope.Subdivision, $scope);
       }
    ]);
})();
