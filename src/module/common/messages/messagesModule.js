(function(angular){
    'use strict';

    var module = angular.module('commons.messages',[
        'angular-growl',
        'dialogs.main',
        'ui.bootstrap'
    ]);

    module.config(['$translateProvider',function($translateProvider){
        $translateProvider.translations('ru',{
            DIALOGS_ERROR: "Ошибка",
            DIALOGS_ERROR_MSG: "Произошла неизвестная ошибка.",
            DIALOGS_CLOSE: "Закрыть",
            DIALOGS_PLEASE_WAIT: "Пожалуйста, подождите",
            DIALOGS_PLEASE_WAIT_ELIPS: "Пожалуйста, подождите...",
            DIALOGS_PLEASE_WAIT_MSG: "Ожидание завершения операции.",
            DIALOGS_PERCENT_COMPLETE: "% завершенно",
            DIALOGS_NOTIFICATION: "Уведомление",
            DIALOGS_NOTIFICATION_MSG: "Неизвестное уведомление приложения.",
            DIALOGS_CONFIRMATION: "Подтверждение",
            DIALOGS_CONFIRMATION_MSG: "Требуется подтверждение.",
            DIALOGS_OK: "OK",
            DIALOGS_YES: "Да",
            DIALOGS_NO: "Нет"
        });
    }]);

    module.config(['growlProvider', function(growlProvider) {
        growlProvider.onlyUniqueMessages(false);
        growlProvider.globalTimeToLive(10 * 1000);
    }]);

    module.provider('commons.messages.dialog',function provider() {

        this.$get = ['dialogs',
            function factory(dialogs) {

                return {
                    error:dialogs.error,
                    notify:dialogs.notify,
                    confirm:dialogs.confirm
                };
            }
        ];
    });

    module.provider('commons.messages.notify',function provider() {
        this.$get = ['growl',
            function factory(growl) {

                return {
                    success:growl.success,
                    info:growl.info,
                    warning:growl.warning,
                    error:growl.error
                };
            }
        ];
    });

    module.provider('commons.messages',function provider() {
        var ng$log;
        var logObject = {};

        function argumentsToArrayAndAddDate(fnArguments) {
            var arg = [new Date()];
            for (var i = 0; i < fnArguments.length; i++) {
                arg.push(fnArguments[i]);
            }
            return arg;
        }

        var noOperation = function () {
        };

        var debug = function(){
            ng$log.debug.apply(null, argumentsToArrayAndAddDate(arguments));
        };

        var info = function(){
            ng$log.info.apply(this, argumentsToArrayAndAddDate(arguments));
        };

        var warn = function(){
            ng$log.warn.apply(this, argumentsToArrayAndAddDate(arguments));
        };

        var error = function(){
            ng$log.error.apply(this, argumentsToArrayAndAddDate(arguments));
        };

        var log = function(){
            ng$log.log.apply(this, argumentsToArrayAndAddDate(arguments));
        };

        this.setDebugLevel = function(){
            logObject.debug = debug;
            logObject.info = info;
            logObject.warn = warn;
            logObject.error = error;
            logObject.log = log;
        };

        this.setInfoLevel = function(){
            logObject.debug = noOperation;
            logObject.info = info;
            logObject.warn = warn;
            logObject.error = error;
            logObject.log = log;
        };

        this.setWarningLevel = function(){
            logObject.debug = noOperation;
            logObject.info = noOperation;
            logObject.warn = warn;
            logObject.error = error;
            logObject.log = log;
        };

        this.setErrorLevel = function(){
            logObject.debug = noOperation;
            logObject.info = noOperation;
            logObject.warn = noOperation;
            logObject.error = error;
            logObject.log = log;
        };

        this.setLogOff = function(){
            logObject.debug = noOperation;
            logObject.info = noOperation;
            logObject.warn = noOperation;
            logObject.error = noOperation;
            logObject.log = noOperation;
        };

        this.setDebugLevel();

        this.$get = ['$log', 'commons.messages.dialog', 'commons.messages.notify',
            function factory($log, dialog, notify) {
                ng$log = $log;

                return {
                    notify:{
                        success:notify.success,
                        info:notify.info,
                        warning:notify.warning,
                        error:notify.error
                    },
                    dialog:{
                        error:dialog.error,
                        notify:dialog.notify,
                        confirm:dialog.confirm
                    },
                    log:logObject
                };
            }
        ];
    });


})(angular);

