(function(angular){
    'use strict';

    var module = angular.module('common.cades',[
        'ui.router'
    ]);

    module.config(['$stateProvider',
        function ($stateProvider) {
            $stateProvider
                .state('cades', {
                    url:'/cades',
                    templateUrl: 'common/cades/cades.html',
                    controller:"CadesCtrl"
                });
        }
    ]);

    module.controller("CadesCtrl", [
                '$rootScope',
                '$scope',
                '$state',
                'commons.messages',
                function ($rootScope, $scope, $state, messages) {
                    var checkErrorSignPlugin = function() {
                        var canPromise = !!window.Promise;
                        if (canPromise) {
                            cadesplugin.then(function () {
                                    Common_CheckForPlugIn();
                                },
                                function (error) {
                                    messages.log.info(error);
                                    $scope.checkErrorSignPlugin = true;
                                }
                            );
                        } else {
                            window.addEventListener("message", function (event) {
                                    if (event.data == "cadesplugin_loaded") {
                                        CheckForPlugIn_NPAPI();
                                    } else if (event.data == "cadesplugin_load_error") {
                                        $scope.checkErrorSignPlugin = true;
                                    }
                                },
                                false);
                        }
                    };

                    checkErrorSignPlugin();



                    $scope.checkErrorSign = $scope.checkErrorSignPlugin || $scope.checkErrorSignMainSert;

//                    messages.log.info($rootScope.data.create.xml);

            }
        ]);
})(angular);