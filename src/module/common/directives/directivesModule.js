(function () {
    'use strict';

    var module = angular.module('common.directives', [
    ]);

    // Проверяет, удовлетворяет ли значение value числовому шаблону: 0 или знаковое число, начинающееся с 1.
    function isInteger(value) {
        if (angular.isString(value) && value != ''){
            return /^0?$|^\-?[1-9]+\d*$/.test(value);
        }
        return false;
    }

    // Проверяет, удовлетворяет ли значение value числовому шаблону: 0 или знаковое число, начинающееся с 1 с точкой или без.
    function isDecimal(value) {
        return /^(\-?\d+(\.\d+)?)?$/.test(value);
    }

    // Зачение value должно быть меньше maxInclusive
    function validateMaxInclusive(value, maxInclusive) {
        if (angular.isDefined(value) && angular.isDefined(maxInclusive)) {
            if (isInteger(value) && isInteger(maxInclusive)) {
                return +value <= +maxInclusive;
            }
        }
        return false;
    }

    // Зачение value должно быть больше minInclusive
    function validateMinInclusive(value, minInclusive) {
        if (angular.isDefined(value) && angular.isDefined(minInclusive)) {
            if (isInteger(value) && isInteger(minInclusive)) {
                return +value >= +minInclusive;
            }
        }
        return false;
    }

    // Зачение value должно быть меньше либо равно maxExclusive
    function validateMaxExclusive(value, maxExclusive) {
        if (angular.isDefined(value) && angular.isDefined(maxExclusive)) {
            if (isInteger(value) && isInteger(maxExclusive)) {
                return +value < +maxExclusive;
            }
        }
        return false;
    }

    // Зачение value должно быть больше либо равно атрибуту minExclusive
    function validateMinExclusive(value, minExclusive) {
        if (angular.isDefined(value) && angular.isDefined(minExclusive)) {
            if (isInteger(value) && isInteger(minExclusive)) {
                return +value > +minExclusive;
            }
        }
        return false;
    }

    // Количество символов значения value должно быть меньше либо равно totalDigits
    function validateTotalDigits(value, totalDigits) {
        if (angular.isDefined(value) && angular.isDefined(totalDigits)){
            if (value != null && isInteger(totalDigits)) {
                return value.length <= +totalDigits;
            }
        }
        return false;
    }


    // Количество символов значения value после запятой должно быть меньше либо равно fractionDigits
    function validateFractionDigits(value, fractionDigits) {
        if (angular.isDefined(value) && angular.isDefined(fractionDigits)) {
            if (isDecimal(value) && isInteger(fractionDigits) && value.lastIndexOf('.') != -1) {
                var fractionValue = value.substring(value.indexOf('.') + 1);
                return fractionValue.length <= +fractionDigits;
            }
        }
        return false;
    }

    // Количество символов значения value должно быть меньше либо равно length
    function validateLength(value, length) {
        if (angular.isString(value) && angular.isDefined(length)) {
            if (value != null && isInteger(length)) {
                return value.length == +length;
            }
        }
        return false;
    }

    //// Количество символов значения value должно быть больше либо равно minLength
    //function validateMinLength(value, minLength) {
    //    if (angular.isDefined(value) && angular.isDefined(minLength)) {
    //        if (isInteger(minLength)) {
    //            return value.length >= +minLength;
    //        }
    //    }
    //    return false;
    //}
    //
    //// Количество символов значения value должно быть больше либо равно maxLength
    //function validateMaxLength(value, maxLength) {
    //    if (angular.isDefined(value) && angular.isDefined(maxLength)) {
    //        if (isInteger(maxLength)) {
    //            return value.length <= +maxLength;
    //        }
    //    }
    //    return false;
    //}

    function validateInnFormat(inn) {
        if ($.isEmptyObject(inn) || !/^\d{10}$|^\d{12}$/.test(inn)) {
            return false;
        }
        inn = inn.split('').map(Number);
        var checkNumbers = [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8];

        if (inn.length == 12) {
            var first = checkNumbers.shift();
            if (!checkRemainderOfDivision()) {
                return false;
            }

            checkNumbers.unshift(first);
            return checkRemainderOfDivision();

        } else if (inn.length == 10) {
            checkNumbers = checkNumbers.slice(2);
            return checkRemainderOfDivision();
        }
        return false;

        function checkRemainderOfDivision() {
            var checkSum = 0;
            for (var i = 0; i < checkNumbers.length; i++) {
                checkSum += checkNumbers[i] * inn[i];
            }
            return checkSum % 11 % 10 == inn[i];
        }
    }

    function validateOgrnFormat(ogrn) {
        if ($.isEmptyObject(ogrn) || !/^\d{13}$/.test(ogrn)) {
            return false;
        }
        var control = ogrn.slice(-1);
        ogrn = ogrn.slice(0, -1);
        return ogrn % 11 % 10 == control;
    }

    module.directive('xsType', function () {
        return {
            require:'ngModel',
            link:function (scope, elm, attrs, ngModelCtrl) {
                function checkIntegerFormat() {
                    ngModelCtrl.$parsers.unshift(function (viewValue) {
                        ngModelCtrl.$setValidity('dataTypeInteger', isInteger(viewValue));
                        return viewValue;
                    });

                    ngModelCtrl.$formatters.unshift(function (modelValue) {
                        ngModelCtrl.$setValidity('dataTypeInteger', isInteger(modelValue));
                        return modelValue;
                    });
                }

                function checkDecimalFormat() {
                    ngModelCtrl.$parsers.unshift(function (viewValue) {
                        ngModelCtrl.$setValidity('dataTypeDecimal', isDecimal(viewValue));
                        return viewValue;
                    });

                    ngModelCtrl.$formatters.unshift(function (modelValue) {
                        ngModelCtrl.$setValidity('dataTypeDecimal', isDecimal(modelValue));
                        return modelValue;
                    });
                }

                function checkMaxInclusive() {
                    var maxInclusive = attrs.maxInclusive;
                    if (isInteger(maxInclusive)) {
                        ngModelCtrl.$parsers.unshift(function (viewValue) {
                            ngModelCtrl.$setValidity('maxInclusive', validateMaxInclusive(viewValue, maxInclusive));
                            return viewValue;
                        });

                        ngModelCtrl.$formatters.unshift(function (modelValue) {
                            ngModelCtrl.$setValidity('maxInclusive', validateMaxInclusive(modelValue, maxInclusive));
                            return modelValue;
                        });
                    }
                }

                function checkMinInclusive() {
                    var minInclusive = attrs.minInclusive;
                    if (isInteger(minInclusive)) {
                        ngModelCtrl.$parsers.unshift(function (viewValue) {
                            ngModelCtrl.$setValidity('minInclusive', validateMinInclusive(viewValue, minInclusive));
                            return viewValue;
                        });

                        ngModelCtrl.$formatters.unshift(function (modelValue) {
                            ngModelCtrl.$setValidity('minInclusive', validateMinInclusive(modelValue, minInclusive));
                            return modelValue;
                        });
                    }
                }

                function checkMaxExclusive() {
                    var maxExclusive = attrs.maxExclusive;
                    if (isInteger(maxExclusive)) {
                        ngModelCtrl.$parsers.unshift(function (viewValue) {
                            ngModelCtrl.$setValidity('maxExclusive', validateMaxExclusive(viewValue, maxExclusive));
                            return viewValue;
                        });

                        ngModelCtrl.$formatters.unshift(function (modelValue) {
                            ngModelCtrl.$setValidity('maxExclusive', validateMaxExclusive(modelValue, maxExclusive));
                            return modelValue;
                        });
                    }
                }

                function checkMinExclusive() {
                    var minExclusive = attrs.minExclusive;
                    if (isInteger(minExclusive)) {
                        ngModelCtrl.$parsers.unshift(function (viewValue) {
                            ngModelCtrl.$setValidity('minExclusive', validateMinExclusive(viewValue, minExclusive));
                            return viewValue;
                        });

                        ngModelCtrl.$formatters.unshift(function (modelValue) {
                            ngModelCtrl.$setValidity('minExclusive', validateMinExclusive(modelValue, minExclusive));
                            return modelValue;
                        });
                    }
                }

                function checkTotalDigits() {
                    var totalDigits = attrs.totalDigits;
                    if (isInteger(totalDigits)) {
                        ngModelCtrl.$parsers.unshift(function (viewValue) {
                            ngModelCtrl.$setValidity('totalDigits', validateTotalDigits(viewValue, totalDigits));
                            return viewValue;
                        });

                        ngModelCtrl.$formatters.unshift(function (modelValue) {
                            ngModelCtrl.$setValidity('totalDigits', validateTotalDigits(modelValue, totalDigits));
                            return modelValue;
                        });
                    }
                }

                function checkFractionDigits() {
                    var fractionDigits = attrs.fractionDigits;
                    if (isInteger(fractionDigits)) {
                        ngModelCtrl.$parsers.unshift(function (viewValue) {
                            ngModelCtrl.$setValidity('fractionDigits', validateFractionDigits(viewValue, fractionDigits));
                            return viewValue;
                        });

                        ngModelCtrl.$formatters.unshift(function (modelValue) {
                            ngModelCtrl.$setValidity('fractionDigits', validateFractionDigits(modelValue, fractionDigits));
                            return modelValue;
                        });
                    }
                }

                function checkLength() {
                    var length = attrs.length;
                    if (isInteger(length)) {
                        ngModelCtrl.$parsers.unshift(function (viewValue) {
                            ngModelCtrl.$setValidity('length', validateLength(viewValue, length));
                            return viewValue;
                        });

                        ngModelCtrl.$formatters.unshift(function (modelValue) {
                            ngModelCtrl.$setValidity('length', validateLength(modelValue, length));
                            return modelValue;
                        });
                    }
                }

                //function checkMinLength() {
                //    var minLength = attrs.minLength;
                //    if (isInteger(minLength)) {
                //        ngModelCtrl.$parsers.unshift(function (viewValue) {
                //            ngModelCtrl.$setValidity('minLength', validateMinLength(viewValue, minLength));
                //            return viewValue;
                //        });
                //
                //        ngModelCtrl.$formatters.unshift(function (modelValue) {
                //            ngModelCtrl.$setValidity('minLength', validateMinLength(modelValue, minLength));
                //            return modelValue;
                //        });
                //    }
                //}
                //
                //function checkMaxLength() {
                //    var maxLength = attrs.maxLength;
                //    if (isInteger(maxLength)) {
                //        ngModelCtrl.$parsers.unshift(function (viewValue) {
                //            ngModelCtrl.$setValidity('maxLength', validateMaxLength(viewValue, maxLength));
                //            return viewValue;
                //        });
                //
                //        ngModelCtrl.$formatters.unshift(function (modelValue) {
                //            ngModelCtrl.$setValidity('maxLength', validateMaxLength(modelValue, maxLength));
                //            return modelValue;
                //        });
                //    }
                //}

                function checkInn() {
                    var check = attrs.check;
                    if (angular.isDefined(check) && check == 'INN') {
                        ngModelCtrl.$parsers.unshift(function (viewValue) {
                            ngModelCtrl.$setValidity('inn', validateInnFormat(viewValue));
                            return viewValue;
                        });

                        ngModelCtrl.$formatters.unshift(function (modelValue) {
                            ngModelCtrl.$setValidity('inn', validateInnFormat(modelValue));
                            return modelValue;
                        });
                    }
                }

                function checkOgrn() {
                    var check = attrs.check;
                    if (angular.isDefined(check) && check == 'OGRN') {
                        ngModelCtrl.$parsers.unshift(function (viewValue) {
                            ngModelCtrl.$setValidity('ogrn', validateOgrnFormat(viewValue));
                            return viewValue;
                        });

                        ngModelCtrl.$formatters.unshift(function (modelValue) {
                            ngModelCtrl.$setValidity('ogrn', validateOgrnFormat(modelValue));
                            return modelValue;
                        });
                    }
                }





                if (attrs.xsType == 'xs:string'){
                    checkLength();
                    //checkMinLength();
                    //checkMaxLength();
                    checkInn();
                    checkOgrn();
                } else if (attrs.xsType == 'xs:decimal'){
                    checkDecimalFormat();
                    checkMaxInclusive();
                    checkMinInclusive();
                    checkMaxExclusive();
                    checkMinExclusive();
                    checkTotalDigits();
                    checkFractionDigits();
                } else if (attrs.xsType == 'xs:integer'){
                    checkIntegerFormat();
                    checkMaxInclusive();
                    checkMinInclusive();
                    checkMaxExclusive();
                    checkMinExclusive();
                    checkTotalDigits();
                } else if (attrs.xsType == 'xs:date'){

                } else {
                    console.log("In XSD type new value...");
                }

            }
        };
    });
})();