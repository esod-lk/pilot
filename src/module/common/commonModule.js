(function () {
    'use strict';

    var module = angular.module("common", [
        'ui.router',
        'ngFormValidation',
        'common.directives',
        'commons.messages',
        'common.cades'
    ]);

    module.config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('root', {
                    templateUrl: 'common/index.html'
                });
            $urlRouterProvider.otherwise('/');
        }
    ]);

    module.provider('common.xmlService', function () {
        this.$get = [
            function factory() {

                return {
                    fixLoadedXmlForControllers:{
                        fixPhone: function (element, $scope) {
                            if (!angular.isObject(element.Phones)){
                                element.Phones = {};
                            }

                            if (!angular.isArray(element.Phones.Phone)){
                                if (angular.isObject(element.Phones.Phone)){
                                    element.Phones.Phone = [
                                        element.Phones.Phone
                                    ];
                                }else{
                                    element.Phones.Phone = [];
                                }
                            }

                            $scope.addPhone = function () {
                                element.Phones.Phone.push({});
                            };
                        },
                        fixEmail: function (element, $scope) {
                            $scope.emails = [];
                            if (!angular.isObject(element['E-Mails'])){
                                element['E-Mails'] = {};
                            }

                            if (!angular.isArray(element['E-Mails']['E-Mail'])){
                                if (angular.isString(element['E-Mails']['E-Mail'])){
                                    element['E-Mails']['E-Mail'] = [
                                        element['E-Mails']['E-Mail']
                                    ];
                                }else{
                                    element['E-Mails']['E-Mail'] = [];
                                }
                            }
                            for (var i=0;i<element['E-Mails']['E-Mail'].length; i++){
                                $scope.emails.push({value:element['E-Mails']['E-Mail'][i]})
                            }


                            $scope.addEmail = function () {
                                element['E-Mails']['E-Mail'].push("");
                                $scope.emails.push({value:null});
                            };

                            $scope.changEmail = function (index) {
                                element['E-Mails']['E-Mail'][index] = $scope.emails[index].value;
                            };

                        },
                        addFaxFunction: function (element, $scope) {
                            $scope.notFax = angular.isUndefined(element.Fax);

                            $scope.changeFax = function () {
                                if ($scope.notFax){
                                    element.Fax = {};
                                }else{
                                    delete element.Fax;
                                }
                            };
                        }
                    }
                };
            }
        ];
    });

})();