/*

 [TREE attribute]
 folder-tree-view: the folderTreeView directive
 tree-id : each tree's unique id.
 tree-model : the tree model on $scope.
 node-id : each node's id
 node-label : each node's label
 node-children: each node's children

 <div
 data-folder-tree-view="true"
 data-tree-id="tree"
 data-tree-model="roleList"
 data-node-id="roleId"
 data-node-label="roleName"
 data-node-children="children" >
 </div>
 */

(function (angular) {
    'use strict';

    angular.module('folderTreeView', []).directive('treeModel', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                //tree id
                var treeId = attrs.treeId;

                //tree model
                var treeModel = attrs.treeModel;

                //node id
                var nodeId = attrs.nodeId || 'id';

                //node label
                var nodeLabel = attrs.nodeLabel || 'label';

                //children
                var nodeChildren = attrs.nodeChildren || 'children';

                //tree template
                var template =
                    '<ul>' +
                    '<li data-ng-class="node.classValid" class="validColor" data-ng-repeat="node in ' + treeModel + '">' +
                    '<i class="fa fa-folder-o icon-folder-close-alt text-warning" data-ng-show="node.collapsed" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
                    '<i class="fa fa-folder-open-o icon-folder-open-alt text-warning" data-ng-show="!node.collapsed" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
                    //' <span data-ng-class="node.selected" data-ng-click="' + treeId + '.selectNodeLabel(node)" data-ng-dblclick="' + treeId + '.selectNodeHead(node)">{{node.' + nodeLabel + '}}</span>' +
                    ' <span data-ui-sref="{{node.state}}" data-ui-sref-active="bg-primary" data-ng-click="' + treeId + '.selectNodeLabel(node)" data-ng-dblclick="' + treeId + '.selectNodeHead(node)">{{node.' + nodeLabel + '}}</span>' +
                    ' <i data-ng-class="node.iconValidType"></i>' +
                    '<div data-ng-hide="node.collapsed" data-tree-id="' + treeId + '" data-tree-model="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + '></div>' +
                    '</li>' +
                    '</ul>';


                //check tree id, tree model
                if (treeId && treeModel) {

                    //root node
                    if (attrs.folderTreeView) {

                        //create tree object if not exists
                        scope[treeId] = scope[treeId] || {};

                        //if node head clicks,
                        scope[treeId].selectNodeHead = scope[treeId].selectNodeHead || function (selectedNode) {

                            //Collapse or Expand
                            selectedNode.collapsed = !selectedNode.collapsed;
                        };

                        //if node label clicks,
                        scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function (selectedNode) {
                            var otherNode = true;

                            //remove highlight from previous node
                            if (scope[treeId].currentNode && scope[treeId].currentNode.selected) {
                                if (scope[treeId].currentNode == selectedNode) {
                                    otherNode = false;
                                }
                                scope[treeId].currentNode.selected = undefined;
                                scope[treeId].currentNode = undefined;
                            }

                            if (otherNode) {
                                //set highlight to selected node
                                //selectedNode.selected = 'bg-primary';
                                //selectedNode.selected = 'selected';

                                //set currentNode
                                scope[treeId].currentNode = selectedNode;
                            }
                        };
                    }

                    //Rendering template.
                    element.html('').append($compile(template)(scope));
                }
            }
        };
    }]);
})(angular);

